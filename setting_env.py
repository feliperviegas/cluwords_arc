from dotenv import load_dotenv
from os.path import join, dirname
import os


class EnvVariables(object):
    def __init__(self):
        dotenv_path = join(dirname(__file__), '.env')
        load_dotenv(dotenv_path)
        self.mode = os.environ.get("MODE")
        self.n_jobs = int(os.environ.get("N_THREADS"))
        self.clustering_method = os.environ.get("CLUSTERING_METHOD")
        self.embedding_type = bool(os.environ.get("EMBEDDINGS_BIN_TYPE"))
        self.path_to_save_embeddings = os.environ.get("PATH_TO_SAVE_EMBEDDINGS")
        self.path_to_save_datasets = os.environ.get("PATH_TO_SAVE_DATASETS")
        self.vocab_type = bool(os.environ.get("VOCAB_TYPE"))
        self.cluwords_type = os.environ.get("CLUWORDS_TYPE")
        self.svd_latents = float(os.environ.get("SVD_PERC_LANTENTS"))

    def get_mode(self):
        return self.mode

    def get_n_jobs(self):
        return self.n_jobs

    def get_clustering_method(self):
        return self.clustering_method

    def get_embedding_type(self):
        return self.embedding_type

    def get_path_to_save_embeddings(self):
        return self.path_to_save_embeddings

    def get_path_to_save_datasets(self):
        return self.path_to_save_datasets

    def get_vocab_type(self):
        return self.vocab_type

    def get_cluwords_type(self):
        return self.cluwords_type

    def get_svd_latents(self):
        return self.svd_latents


if __name__ == "__main__":
    # execute only if run as a script
    print(EnvVariables().get_mode())
    print(EnvVariables().get_n_jobs())
    print(EnvVariables().get_clustering_method())
    print(EnvVariables().get_embedding_type())
    print(EnvVariables().get_path_to_save_embeddings())
    print(EnvVariables().get_path_to_save_datasets())
    print(EnvVariables().get_vocab_type())

