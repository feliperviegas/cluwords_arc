import hnswlib
import pickle
import numpy as np
from scipy import sparse
import seaborn as sns
import matplotlib.pyplot as plt
from src.main.python.structure.incremental_coo_matrix import IncrementalCOOMatrix


class Hnsw:
    def __init__(self, threshold, k_neighbors, n_threads):
        self.threshold = threshold
        self.k_neighbors = k_neighbors
        self.n_threads = n_threads

    @staticmethod
    def plot_dist(cosine_matrix, filename="embedding_dist.png", percentile=False):
        cosine_vec = cosine_matrix.flatten()
        plt.figure()
        _ = plt.hist(cosine_vec, bins=20)
        title_string = "BERT -- Distribution Cosine Similarity"
        subtitle_string = f"Perc. 75%: α > {np.round_(np.percentile(cosine_vec, 75), 2)} = {np.sum((cosine_vec > np.percentile(cosine_vec, 75)) * 1.)}\n" \
                          f"Perc. 90%: α > {np.round_(np.percentile(cosine_vec, 90), 2)} = {np.sum((cosine_vec > np.percentile(cosine_vec, 90)) * 1.)}\n" \
                          f"Perc. 95%: α > {np.round_(np.percentile(cosine_vec, 95), 2)} = {np.sum((cosine_vec > np.percentile(cosine_vec, 95)) * 1.)}\n" \
                          f"Perc. 99%: α > {np.round_(np.percentile(cosine_vec, 99), 2)} = {np.sum((cosine_vec > np.percentile(cosine_vec, 99)) * 1.)}\n"

        plt.suptitle(title_string, y=1.2, fontsize=18)
        plt.title(subtitle_string, fontsize=10)

        plt.axvline(np.percentile(cosine_vec, 75), color='red')
        plt.axvline(np.percentile(cosine_vec, 90), color='blue')
        plt.axvline(np.percentile(cosine_vec, 95), color='black')
        plt.axvline(np.percentile(cosine_vec, 95), color='gray')
        plt.savefig(filename)
        return

    def fit_semantic_matrix(self, word_vectors, vocabulary_embedding, dataset_name, fold, path_to_save):
        n_words = len(vocabulary_embedding)
        # Declaring index
        p = hnswlib.Index(space='cosine', dim=word_vectors.shape[1])  # possible options are l2, cosine or ip

        # Initializing index - the maximum number of elements should be known beforehand
        p.init_index(max_elements=n_words, ef_construction=200, M=64)

        word_vectors_labels = np.arange(n_words)
        # Element insertion (can be called several times):
        p.add_items(word_vectors, word_vectors_labels)

        # Controlling the recall by setting ef:
        p.set_ef(600)  # ef should always be > k

        p.set_num_threads(self.n_threads)

        # Query dataset, k - number of closest elements (returns 2 numpy arrays)
        labels, distances = p.knn_query(word_vectors, k=self.k_neighbors)

        # self.plot_dist(1. - distances, f"embedding_dist_{fold}.png", percentile=True)

        if self.threshold == None:
            cosine_vec = (1. - distances).flatten()
            self.threshold = np.round(np.percentile(cosine_vec, 95), 2)


        list_cluwords = IncrementalCOOMatrix(shape=(n_words, n_words), dtype=np.float32)

        for word_ref_index in range(0, n_words):
            for index, k in enumerate(labels[word_ref_index]):
                if 1 - distances[word_ref_index][index] >= self.threshold:
                    list_cluwords.append(word_ref_index, k, round(1. - distances[word_ref_index][index], 2))

        # self.plot_dist(list_cluwords.tocsr().todense(), f"embedding_dist_after_filter_{fold}.png")

        np.savez_compressed("{}/{}_cluwords_information_{}.npz".format(path_to_save,
                                                                       dataset_name,
                                                                       fold),
                            index=np.asarray(vocabulary_embedding),
                            cluwords=np.asarray(vocabulary_embedding),
                            k_neighbors=self.k_neighbors,
                            threshold=self.threshold)

        sparse.save_npz("{}/{}_cluwords_cosine_{}.npz".format(path_to_save,
                                                              dataset_name,
                                                              fold), list_cluwords.tocsr())


    def get_threshold(self):
        return self.threshold

