import numpy as np
from scipy import sparse
from sklearn.neighbors import NearestNeighbors
from src.main.python.structure.incremental_coo_matrix import IncrementalCOOMatrix


class KnnCosine:
    def __init__(self, n_threads, threshold=0.4, k_neighbors=500, mode="sparse"):
        self.threshold = threshold
        self.n_threads = n_threads
        self.k_neighbors = k_neighbors
        self.mode = mode
        # print('N Threads: {}'.format(self.n_threads))

    def fit_semantic_matrix(self, word_vectors, vocabulary_embedding, dataset_name, fold, path_to_save):
        n_words = len(vocabulary_embedding)
        nbrs = NearestNeighbors(n_neighbors=self.k_neighbors,
                                algorithm='auto',
                                metric='cosine',
                                n_jobs=self.n_threads).fit(word_vectors)

        distances, indices = nbrs.kneighbors(word_vectors)

        if self.mode == 'sparse':
            self.save_cluwords_csr_matrix(vocabulary_embedding,
                                          n_words,
                                          distances,
                                          indices,
                                          dataset_name,
                                          fold,
                                          path_to_save)
        elif self.mode == 'memmap':
            self._save_cluwords_memmap(vocabulary_embedding,
                                       n_words,
                                       distances,
                                       indices,
                                       dataset_name,
                                       fold,
                                       path_to_save)
        else:
            raise NameError('Error: mode not found')

        return

    def _save_cluwords_memmap(self, vocabulary_embedding, n_words,
                              distances, indices, dataset_name, fold,
                              path_to_save):
        sim_matrix_memmap = np.memmap(filename='{}/{}_cluwords_information_{}.mem'.format(path_to_save,
                                                                                          dataset_name,
                                                                                          fold),
                                      dtype='float32',
                                      mode='w+',
                                      shape=(n_words,
                                             n_words))

        sim_matrix_bin_memmap = np.memmap(filename="{}/{}_cluwords_cosine_bin_{}.mem".format(path_to_save,
                                                                                             dataset_name,
                                                                                             fold),
                                          dtype='float32',
                                          mode='w+',
                                          shape=(n_words,
                                                 n_words))

        for word_ref_index in range(0, n_words):
            for index, word_neighbor in enumerate(indices[word_ref_index]):
                if 1 - distances[word_ref_index][index] >= self.threshold:
                    sim_matrix_memmap[word_ref_index, word_neighbor] = round(1 - distances[word_ref_index][index], 2)
                    sim_matrix_bin_memmap[word_ref_index, word_neighbor] = 1.
                else:
                    sim_matrix_memmap[word_ref_index, word_neighbor] = .0
                    sim_matrix_bin_memmap[word_ref_index, word_neighbor] = .0

        np.savez_compressed("{}/{}_cluwords_information_{}.npz".format(path_to_save,
                                                                       dataset_name,
                                                                       fold),
                            index=np.asarray(vocabulary_embedding),
                            cluwords=np.asarray(vocabulary_embedding),
                            k_neighbors=self.k_neighbors,
                            threshold=self.threshold)
        sim_matrix_memmap.flush()
        sim_matrix_bin_memmap.flush()
        return

    def save_cluwords_csr_matrix(self, vocabulary_embedding, n_words,
                                 distances, indices, dataset_name, fold,
                                 path_to_save):
        list_cluwords = IncrementalCOOMatrix(shape=(n_words, n_words), dtype=np.float32)
        # TODO - Check if it is not in use
        # list_cluwords_bin = IncrementalCOOMatrix(shape=(n_words, n_words), dtype=np.float32)

        for word_ref_index in range(0, n_words):
            for index, k in enumerate(indices[word_ref_index]):
                if 1 - distances[word_ref_index][index] >= self.threshold:
                    list_cluwords.append(word_ref_index, k, round(1. - distances[word_ref_index][index], 2))
                    # list_cluwords.append(word_ref_index, k, 1. - round(distances[word_ref_index][index], 2))

                    # TODO - Check if it is not in use
                    # list_cluwords_bin.append(word_ref_index, k, round(1))

        np.savez_compressed("{}/{}_cluwords_information_{}.npz".format(path_to_save,
                                                                       dataset_name,
                                                                       fold),
                            index=np.asarray(vocabulary_embedding),
                            cluwords=np.asarray(vocabulary_embedding),
                            k_neighbors=self.k_neighbors,
                            threshold=self.threshold)

        sparse.save_npz("{}/{}_cluwords_cosine_{}.npz".format(path_to_save,
                                                              dataset_name,
                                                              fold), list_cluwords.tocsr())
        # TODO - Check if it is not in use
        # sparse.save_npz("{}/{}_cluwords_cosine_bin_{}.npz".format(path_to_save,
        #                                                           dataset_name,
        #                                                           fold), list_cluwords_bin.tocsr())
        return
