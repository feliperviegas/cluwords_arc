from src.main.python.clustering.knn_algorithm import KnnCosine
from src.main.python.clustering.nmshnsw import NmsHnsw
from src.main.python.clustering.hnsw import Hnsw


class GenerateSemanticInformation:
    def __init__(self, dataset_name, fold, algorithm, word_vectors, vocabulary_embedding, k_neighbors,
                 threshold, mode='memmap', n_jobs=1, verbose=False, path_to_save_results="./"):
        if verbose:
            print('K: {}'.format(k_neighbors))
            print('Cossine: {}'.format(threshold))

        self.threshold = threshold

        if algorithm == 'knn_cosine':
            if self.threshold == None:
                self.threshold = .4
            
            knn = KnnCosine(threshold=self.threshold,
                            n_threads=n_jobs,
                            k_neighbors=k_neighbors,
                            mode=mode)
            knn.fit_semantic_matrix(word_vectors=word_vectors,
                                    vocabulary_embedding=vocabulary_embedding,
                                    dataset_name=dataset_name,
                                    fold=fold,
                                    path_to_save=path_to_save_results)
        elif algorithm == "nms_hnsw":
            if self.threshold == None:
                self.threshold = .4

            hnsw = NmsHnsw(threshold=self.threshold,
                           n_threads=n_jobs,
                           k_neighbors=k_neighbors)
            hnsw.fit_semantic_matrix(word_vectors=word_vectors,
                                     vocabulary_embedding=vocabulary_embedding,
                                     dataset_name=dataset_name,
                                     fold=fold,
                                     path_to_save=path_to_save_results)
        elif algorithm == "hnsw":
            hnsw = Hnsw(threshold=self.threshold, 
                        n_threads=n_jobs, 
                        k_neighbors=k_neighbors)
            hnsw.fit_semantic_matrix(word_vectors=word_vectors,
                                     vocabulary_embedding=vocabulary_embedding,
                                     dataset_name=dataset_name,
                                     fold=fold,
                                     path_to_save=path_to_save_results)

            self.threshold = hnsw.get_threshold()

        else:
            raise Exception('Invalid method.')

    def get_threshold(self):
        return self.threshold

    # Keep to use later in the weighting step
    # def _cluwords_idf(self, data):
    #     print('Read data')
    #     tf = self.raw_tf(binary=True, dt=np.float32, data=data)
    #
    #     if self.mode == "memmap":
    #         print('Dot tf and hyp_aux')
    #         tf_memmap = np.memmap('tf_document', dtype='float32', mode='w+', shape=(tf.shape[0], tf.shape[1]))
    #         tf_arr = tf
    #         tf_memmap[:] = tf_arr[:]
    #         _dot = tf_memmap.dot(self.similarity_matrix.transpose())
    #         print('Dot tf and bin hyp_aux')
    #         _dot_bin = tf_memmap.dot(self.similarity_matrix_bin.transpose())
    #     else:
    #         print(f"Shape Doc {tf.shape}")
    #         tf = sparse.csr_matrix(tf.copy())
    #         print('Dot tf and hyp_aux')
    #         _dot = tf.dot(sparse.csr_matrix.transpose(self.similarity_matrix))  # np.array n_documents x n_cluwords
    #         print('Dot tf and bin hyp_aux')
    #         _dot_bin = tf.dot(sparse.csr_matrix.transpose(self.similarity_matrix_bin))
    #
    #     print('Divide _dot and _dot_bin')
    #     with warnings.catch_warnings():
    #         warnings.simplefilter("ignore")
    #         mu_hyp = np.nan_to_num(np.divide(_dot, _dot_bin))
    #
    #     print('Sum')
    #     self.cluwords_idf = np.sum(mu_hyp, axis=0)
    #     print('log')
    #     self.cluwords_idf = np.log10(np.divide(self.n_documents, self.cluwords_idf))
    #     print('IDF shape {}'.format(self.cluwords_idf.shape))
