import nmslib
import numpy as np
from scipy import sparse
from src.main.python.structure.incremental_coo_matrix import IncrementalCOOMatrix


class NmsHnsw:
    def __init__(self, threshold, k_neighbors, n_threads):
        self.threshold = threshold
        self.k_neighbors = k_neighbors
        self.n_threads = n_threads

    def fit_semantic_matrix(self, word_vectors, vocabulary_embedding, dataset_name, fold, path_to_save):
        nn_index = nmslib.init(method='hnsw',
                               space='cosinesimil',
                               data_type=nmslib.DataType.DENSE_VECTOR,
                               dtype=nmslib.DistType.FLOAT)
        nn_index.addDataPointBatch(data=word_vectors)
        nn_index.createIndex(print_progress=True)

        Xn = nn_index.knnQueryBatch(word_vectors, k=self.k_neighbors, num_threads=self.n_threads)

        n_words = len(vocabulary_embedding)
        list_cluwords = IncrementalCOOMatrix(shape=(n_words, n_words), dtype=np.float32)

        for ref_index in range(0, len(Xn)):
            word_neighbor, distances = Xn[ref_index]
            word_indexes = np.argwhere((1. - distances) >= self.threshold)
            for index in word_indexes:
                list_cluwords.append(ref_index, word_neighbor[index][0], round(1. - distances[index][0], 2))

        np.savez_compressed("{}/{}_cluwords_information_{}.npz".format(path_to_save,
                                                                       dataset_name,
                                                                       fold),
                            index=np.asarray(vocabulary_embedding),
                            cluwords=np.asarray(vocabulary_embedding),
                            k_neighbors=self.k_neighbors,
                            threshold=self.threshold)

        sparse.save_npz("{}/{}_cluwords_cosine_{}.npz".format(path_to_save,
                                                              dataset_name,
                                                              fold), list_cluwords.tocsr())

