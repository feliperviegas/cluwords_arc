import numpy as np
from gensim.models import KeyedVectors
from gensim.models import FastText
from sklearn.feature_extraction.text import CountVectorizer
import torch
from transformers import BertTokenizer
from transformers import BertModel
import logging
import matplotlib.pyplot as plt
# %matplotlib inline


# #----------TO EXECUTE LOCAL--------------------------:
# from BertEmbedding import BertEmbedding
# embedding = BertEmbedding(dataset_path="<TEXT_PATH>", type=<"concat" or "mean" or "sum">)
# #--------------------------------------------------------


class BertEmbedding:
    def __init__(self):
        torch.manual_seed(0)
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased',
                                                       do_lower_case=True)
        self.vocabulary_embedding = None
        self.word_vectors = None

    def get_senteces(self, dataset_path):
        sentences = self.read_raw_dataset(dataset_path)

        return sentences

    def set_tokenize_sentences(self, dataset_path):
        sentences = self.get_senteces(dataset_path)
        tokenized_info = self.tokenize(sentences)
        tokenized_sentences = np.asarray(tokenized_info['tokenized_texts'])
        del tokenized_info
        t_sentences = list()
        for doc in tokenized_sentences:
            t_sentences.append(' '.join(doc))

        del tokenized_sentences
        return t_sentences

    def get_vocabulary(self, dataset_path):
        sentences = self.get_senteces(dataset_path)
        info = self.tokenize(sentences)
        tokens = np.asarray(info['tokenized_texts']).flatten()
        tokens = set(tokens)
        if '[CLS]' in tokens:
            tokens.remove('[CLS]')
        
        if '[PAD]' in tokens:
            tokens.remove('[PAD]')

        if '[SEP]' in tokens:
            tokens.remove('[SEP]')

        return list(tokens)


    def run(self, dataset_path, path_to_save_model, fold, type='concat'):
        sentences = self.get_senteces(dataset_path)
        info = self.tokenize(sentences)
        self.vocabulary_embedding, self.word_vectors = self.build_embedding(info['input_ids'], info['attention_mask'],
                                                                            info['tokenized_texts'],
                                                                            path_to_save_model, type, fold)

    def tokenize(self, sentences):
        # Tokenize all of the sentences and map tokens to word IDs.
        input_ids = []
        attention_mask = list()
        tokenized_texts = list()
        for sent in sentences:
            encoded_dict = self.tokenizer.encode_plus(
                sent,
                add_special_tokens=True,
                max_length=128,
                return_token_type_ids=False,
                padding="max_length",
                truncation=True,
                return_attention_mask=True,
                return_tensors='pt',
            )
            # Save tokens from sentence as a separate array.
            tokenized_texts.append(self.tokenizer.convert_ids_to_tokens(encoded_dict["input_ids"].squeeze()))

            # Add the encoded sentence to the list.
            input_ids.append(encoded_dict['input_ids'])
            attention_mask.append(encoded_dict['attention_mask'])

        # Convert the list into tensor.
        input_ids = torch.cat(input_ids, dim=0)
        attention_mask = torch.cat(attention_mask, dim=0)

        return {'input_ids': input_ids, 'attention_mask': attention_mask, 'tokenized_texts': tokenized_texts}

    def build_embedding(self, input_ids, attention_masks, tokenized_texts, path_to_save_model, type, fold):
        model = BertModel.from_pretrained('bert-base-uncased',
                                          output_hidden_states=True,  # Whether the model returns all hidden-states.
                                          )
        with torch.no_grad():
            outputs = model(input_ids, attention_masks)
            hidden_states = outputs[2]

        print(f"Number of layers: {len(hidden_states)}, (initial embeddings + 12 BERT layers)")
        print(f"Number of batches: {len(hidden_states[0])}")
        print(f"Number of tokens: {len(hidden_states[0][0])}")
        print(f"Number of hidden units: {len(hidden_states[0][0][0])}")

        # Each layer in the list is a torch tensor.
        print(f"Tensor shape for each layer: {hidden_states[0].size()}")

        token_embeddings = torch.stack(hidden_states, dim=0)

        # Swap dimensions, so we get tensors in format: [senteces, tokens, hidden layes, features]
        token_embeddings = token_embeddings.permute(1, 2, 0, 3)
        print(f"Token Embeddings: {token_embeddings.size()}")

        if type == "mean":
            embedding_word, word_input_ids, dimension = self.mean_four_last_layers(input_ids,
                                                                                   token_embeddings,
                                                                                   tokenized_texts)
        elif type == "sum":
            embedding_word, word_input_ids, dimension = self.sum_four_last_layers(input_ids,
                                                                                  token_embeddings,
                                                                                  tokenized_texts)
        elif type == "concat":
            embedding_word, word_input_ids, dimension = self.concatenate_four_last_layers(input_ids,
                                                                                          token_embeddings,
                                                                                          tokenized_texts)
        else:
            raise Exception("Not found type of embedding.")

        del embedding_word['[CLS]']
        del embedding_word['[SEP]']
        del embedding_word['[PAD]']
        word_embeddings = self.gen_word_embeddings(embedding_word)
        vocabulary, word_vectors = self.save_embedding(word_embeddings, dimension, f"{type}-{fold}",
                                                       path_to_save_model)
        del word_embeddings
        return vocabulary, word_vectors

    @staticmethod
    def load_vocabulary(documents):
        dataset_cv = CountVectorizer().fit(documents)
        vocabulary = dataset_cv.get_feature_names()
        return vocabulary

    @staticmethod
    def read_raw_dataset(document_path):
        arq = open(document_path, 'r', encoding="utf-8")
        doc = arq.readlines()
        arq.close()
        documents = list(map(str.rstrip, doc))

        return documents

    def save_embedding(self, word_embeddings, dimension, sufix, path_to_save_model):
        vocabulary = list()
        word_vectors = list()
        with open(f'{path_to_save_model}/word-embedding_{sufix}.txt', 'w') as output_embedding:
            output_embedding.write(f"{len(word_embeddings.keys())},{dimension}\n")
            for token, word_vector in word_embeddings.items():
                vocabulary.append(token)
                output_embedding.write(f"{token}")
                vector = list()
                for latent in word_vector:
                    output_embedding.write(f" {latent}")
                    vector.append(latent)

                word_vectors.append(np.asarray(vector))
                del vector
                output_embedding.write(f"\n")
            output_embedding.close()

        return np.asarray(vocabulary), np.asarray(word_vectors)

    def gen_word_embeddings(self, embedding_word):
        word_embeddings = dict()
        for token, tensor_embeddings in embedding_word.items():
            mean_tensor = torch.mean(torch.stack(tensor_embeddings), dim=0)
            word_embeddings[token] = mean_tensor

        return word_embeddings

    def concatenate_four_last_layers(self, input_ids, token_embeddings, tokenized_texts):
        embedding_word = dict()
        word_input_ids = dict()

        for batch_id in range(token_embeddings.shape[0]):
            # Select Tensors for a sentence
            aux_tensor = token_embeddings[batch_id, :, :, :]
            # Tensor with dimension [tokens x hidden layes x features]
            aux_tensor = torch.squeeze(aux_tensor, dim=0)

            # For each token in the sentence
            for token_id in range(aux_tensor.shape[0]):
                # `token` is a [13 x 768] tensor
                token = aux_tensor[token_id]

                # Concatenate the vectors (that is, append them together) from the last
                # four layers.
                # Each layer vector is 768 values, so `cat_vec` is length 3,072.
                cat_vec = torch.cat((token[-1], token[-2], token[-3], token[-4]), dim=0)

                # Save `cat_vec` into its respective token string
                if token_id < len(tokenized_texts[batch_id]):
                    str_token = tokenized_texts[batch_id][token_id]
                    if str_token not in embedding_word:
                        embedding_word[str_token] = list()

                    embedding_word[str_token].append(cat_vec)
                    if str_token not in word_input_ids:
                        word_input_ids[str_token] = list()

                    word_input_ids[str_token].append(input_ids[batch_id][token_id])

        dimension = token_embeddings.shape[3] * 4
        return embedding_word, word_input_ids, dimension

    def sum_four_last_layers(self, input_ids, token_embeddings, tokenized_texts):
        embedding_word = dict()
        word_input_ids = dict()

        for batch_id in range(token_embeddings.shape[0]):
            # Select Tensors for a sentence
            aux_tensor = token_embeddings[batch_id, :, :, :]
            # Tensor with dimension [tokens x hidden layes x features]
            aux_tensor = torch.squeeze(aux_tensor, dim=0)

            # For each token in the sentence
            for token_id in range(aux_tensor.shape[0]):
                # `token` is a [13 x 768] tensor
                token = aux_tensor[token_id]

                # Sum the vectors from the last four layers.
                sum_vec = torch.sum(token[-4:], dim=0)

                # Save `cat_vec` into its respective token string
                if token_id < len(tokenized_texts[batch_id]):
                    str_token = tokenized_texts[batch_id][token_id]
                    if str_token not in embedding_word:
                        embedding_word[str_token] = list()

                    embedding_word[str_token].append(sum_vec)
                    if str_token not in word_input_ids:
                        word_input_ids[str_token] = list()

                    word_input_ids[str_token].append(input_ids[batch_id][token_id])

        dimension = token_embeddings.shape[3]
        return embedding_word, word_input_ids, dimension

    def mean_four_last_layers(self, input_ids, token_embeddings, tokenized_texts):
        embedding_word = dict()
        word_input_ids = dict()

        for batch_id in range(token_embeddings.shape[0]):
            # Select Tensors for a sentence
            aux_tensor = token_embeddings[batch_id, :, :, :]
            # Tensor with dimension [tokens x hidden layes x features]
            aux_tensor = torch.squeeze(aux_tensor, dim=0)

            # For each token in the sentence
            for token_id in range(aux_tensor.shape[0]):
                # `token` is a [13 x 768] tensor
                token = aux_tensor[token_id]

                # Sum the vectors from the last four layers.
                mean_vec = torch.mean(token[-4:], dim=0)

                # Save `cat_vec` into its respective token string
                if token_id < len(tokenized_texts[batch_id]):
                    str_token = tokenized_texts[batch_id][token_id]
                    if str_token not in embedding_word:
                        embedding_word[str_token] = list()

                    embedding_word[str_token].append(mean_vec)
                    if str_token not in word_input_ids:
                        word_input_ids[str_token] = list()

                    word_input_ids[str_token].append(input_ids[batch_id][token_id])

        dimension = token_embeddings.shape[3]
        return embedding_word, word_input_ids, dimension

    def get_word_vectors(self):
        return self.word_vectors, self.vocabulary_embedding

    def get_n_words(self):
        return len(self.vocabulary_embedding)

