import numpy as np
from gensim.models import KeyedVectors
from gensim.models import FastText
from sklearn.feature_extraction.text import CountVectorizer


class FastTextEmbedding:
    def __init__(self, embedding_file_path, embedding_type,
                 dataset_path, path_to_save_model, embedding_dimension=300, vocabulary=None):
        self.dataset_path = dataset_path
        self.path_to_save_model = path_to_save_model
        self.vocabulary_embedding = list()
        self.word_vectors = list()
        self.n_words = None

        documents = self.read_raw_dataset(dataset_path)
        
        if vocabulary == None:
            vocabulary = self.load_vocabulary(documents)
        

        if embedding_file_path:
            print('Reading embedding...')
            self.model = self.read_embedding(embedding_file_path, embedding_type)
            print('Done')
        else:
            print('Creating embedding...')
            self.model = FastText(documents, vector_size=embedding_dimension,
                                  window=5, min_count=5, workers=4, sg=1)

        print("Filter embedding")
        self.filter_embedding_models(vocabulary=vocabulary)
        print("Done")
        del self.model
        return

    def filter_embedding_models(self, vocabulary):
        self.vocabulary_embedding = list()
        self.word_vectors = list()
        for word in vocabulary:
            if word in self.model:
                self.vocabulary_embedding.append(word)
                self.word_vectors.append(np.asarray(self.model[word], dtype=np.float32))

        self.n_words = len(self.vocabulary_embedding)  # Number of words selected
        self.word_vectors = np.array(self.word_vectors)
        self.vocabulary_embedding = np.array(self.vocabulary_embedding)
        return

    @staticmethod
    def read_embedding(embedding_file_path, binary):
        model = KeyedVectors.load_word2vec_format(embedding_file_path, binary=binary)
        return model

    @staticmethod
    def load_vocabulary(documents):
        dataset_cv = CountVectorizer().fit(documents)
        vocabulary = dataset_cv.get_feature_names()
        return vocabulary

    @staticmethod
    def read_raw_dataset(document_path):
        arq = open(document_path, 'r', encoding="utf-8")
        doc = arq.readlines()
        arq.close()
        documents = list(map(str.rstrip, doc))

        return documents

    def get_n_words(self):
        return self.n_words

    def get_word_vectors(self):
        return self.word_vectors, self.vocabulary_embedding
