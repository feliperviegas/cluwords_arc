import numpy as np
from scipy import sparse


# Need to double-check the source code and test.
class MutualInformationStep(object):
    def __int__(self, is_fit=True):
        self.is_fit = is_fit
        self.probability_term = None
        self.frequency_term = None

    def compute_norm_cond_mi(self, data, data_class, path_to_save='./', dataset_name='',
                             class_id=0, fold=0, log_file=True):
        if self.is_fit:
            try:
                self._compute_cond_mutual_info(data=data, class_id=class_id, data_class=data_class)
            except Exception as e:
                print('Error: {}'.format(e))

            if log_file:
                print('Writing log file...')
                sparse.save_npz('{}/{}_cluwords_ncmi_{}_{}.npz'.format(path_to_save,
                                                                       dataset_name,
                                                                       class_id,
                                                                       fold), sparse.csr_matrix(self.hyp_mutual_info))
        else:
            raise NameError('Must fit model.')

        return

    @staticmethod
    def conditional_entropy(joint_proba_absence, joint_proba_presence, proba_presence):
        conditional_proba_presence = joint_proba_presence / proba_presence if proba_presence > .0 else .0
        conditional_proba_absence = joint_proba_absence / proba_presence if proba_presence > .0 else .0
        part_presence = - conditional_proba_presence * np.log2(conditional_proba_presence) \
            if conditional_proba_presence > .0 else .0
        part_absence = - conditional_proba_absence * np.log2(conditional_proba_absence) \
            if conditional_proba_absence > .0 else .0

        # if round(conditional_proba_presence, 4) > 1.:
        #     print('<conditional_proba_presence> P(x|y):{} P(x,y):{} P(y):{}'.format(conditional_proba_presence,
        #                                                                             joint_proba_presence,
        #                                                                             proba_presence))
        #
        # if round(conditional_proba_absence, 4) > 1.:
        #     print('<conditional_proba_abscence> P(x|y):{} P(x,y):{} P(y):{}'.format(conditional_proba_absence,
        #                                                                             joint_proba_absence,
        #                                                                             proba_presence))

        # print('<conditional_entropy> {} {} {}'.format(part_presence, part_absence, part_presence + part_absence))
        return part_presence + part_absence

    @staticmethod
    def probability(numerator, denominator):
        return (numerator / denominator) if denominator > .0 else .0

    @staticmethod
    def normalized_mutual_infomation(mutual_information, entropy_term, entropy_class):
        num = 2 * mutual_information
        den = entropy_term + entropy_class
        norm_cond_mutual_info = (num / den) \
            if den != .0 else .0
        return norm_cond_mutual_info

    def compute_probabilities(self, X, y, n_terms, n_classes, n_docs):
        # self.probability_class = np.empty((n_classes, 2), dtype=np.float32)
        # for class_id in range(0, n_classes):
        #     current_class = (y == class_id)
        #     bool_class = (current_class == 1)  # set class = 1
        #     # class = 1
        #     self.probability_class[class_id, 0] = self.probability(np.count_nonzero(bool_class == True), n_docs)
        #     bool_class = (current_class == 0)  # set class = 0
        #     # class = 0
        #     self.probability_class[class_id, 1] = self.probability(np.count_nonzero(bool_class == True), n_docs)

        self.probability_term = np.empty((n_terms, 2), dtype=np.float32)
        self.frequency_term = np.empty((n_terms, 2), dtype=np.float32)
        for term_id in range(0, n_terms):
            current_term = X[:, term_id]
            bool_term = (current_term == 1)  # set term = 1
            # term = 1
            self.probability_term[term_id, 0] = self.probability(np.count_nonzero(bool_term == True), n_docs)
            self.frequency_term[term_id, 0] = np.count_nonzero(bool_term == True)
            bool_term = (current_term == 0)  # set term = 0
            # term = 0
            self.probability_term[term_id, 1] = self.probability(np.count_nonzero(bool_term == True), n_docs)
            self.frequency_term[term_id, 1] = np.count_nonzero(bool_term == True)

    @staticmethod
    def log2(numerator, denominator):
        if numerator != 0 and denominator != 0:
            return np.log2(numerator / denominator)
        else:
            return 0

    def _conditional_mutual_information(self, n_11, n_10, n_01, n_00, n_1_, n__1, n_0_, n__0, n):
        return (n_11 / n) * self.log2((n * n_11), (n_1_ * n__1)) \
               + (n_01 / n) * self.log2((n * n_01), (n_0_ * n__1)) \
               + (n_10 / n) * self.log2((n * n_10), (n_1_ * n__0)) \
               + (n_00 / n) * self.log2((n * n_00), (n_0_ * n__0))

    def _gen_cond_mutual_info(self, X, y, class_id, conditional_term_id, term_id):
        current_class = (y == class_id)
        conditional_term = X[:, conditional_term_id]
        bool_conditional_term = (conditional_term == 1) * 1  # set conditional_term = 1
        confusion_matrix = np.zeros((2, 2))
        current_term = X[:, term_id]

        bool_term_1 = (current_term != 0) * 1  # set term = 1
        bool_term_0 = (current_term == 0) * 1  # set term = 0
        bool_class_1 = (current_class == 1) * 1  # set class = 1
        bool_class_0 = (current_class == 0) * 1  # set class = 0

        sum_cond_term_and_class = np.sum(np.asarray([bool_conditional_term, bool_class_1]), axis=0)
        sum_all = np.sum(np.asarray([sum_cond_term_and_class, bool_term_1]), axis=0)
        confusion_matrix[0, 0] = np.count_nonzero(sum_all == 3)  # term = 1 | class = 1
        del sum_cond_term_and_class
        del sum_all

        sum_cond_term_and_class = np.sum(np.asarray([bool_conditional_term, bool_class_1]), axis=0)
        sum_all = np.sum(np.asarray([sum_cond_term_and_class, bool_term_0]), axis=0)
        confusion_matrix[1, 0] = np.count_nonzero(sum_all == 3)  # term = 0 | class = 1
        del sum_cond_term_and_class
        del sum_all

        sum_cond_term_and_class = np.sum(np.asarray([bool_conditional_term, bool_class_0]), axis=0)
        sum_all = np.sum(np.asarray([sum_cond_term_and_class, bool_term_0]), axis=0)
        confusion_matrix[1, 1] = np.count_nonzero(sum_all == 3)  # term = 0 | class = 0
        del sum_cond_term_and_class
        del sum_all

        sum_cond_term_and_class = np.sum(np.asarray([bool_conditional_term, bool_class_0]), axis=0)
        sum_all = np.sum(np.asarray([sum_cond_term_and_class, bool_term_1]), axis=0)
        confusion_matrix[0, 1] = np.count_nonzero(sum_all == 3)  # term = 1 | class = 0
        del sum_cond_term_and_class
        del sum_all

        if np.sum(confusion_matrix):
            return self._conditional_mutual_information(n_11=confusion_matrix[0, 0],
                                                        n_10=confusion_matrix[0, 1],
                                                        n_01=confusion_matrix[1, 0],
                                                        n_00=confusion_matrix[1, 1],
                                                        n_1_=np.sum(confusion_matrix, axis=1)[0],
                                                        n__1=np.sum(confusion_matrix, axis=0)[0],
                                                        n_0_=np.sum(confusion_matrix, axis=1)[1],
                                                        n__0=np.sum(confusion_matrix, axis=0)[1],
                                                        n=np.sum(confusion_matrix))
        else:
            return 0.0

    def _compute_cond_mutual_info(self, data, data_class, class_id):
        tf = self.raw_tf(data=data)
        y, n_y = self.read_input(file=data_class)
        y = np.array(y, dtype=np.int)
        self.hyp_mutual_info = np.zeros((len(self.vocab_cluwords), len(self.vocab)), dtype=np.float32)
        for w in range(0, len(self.vocab_cluwords)):
            nonzero_indices = np.nonzero(self.similarity_matrix[w])
            for cond_term_id in nonzero_indices[0]:
                self.hyp_mutual_info[w][cond_term_id] = self._gen_cond_mutual_info(X=tf,
                                                                                   y=y,
                                                                                   class_id=class_id,
                                                                                   conditional_term_id=cond_term_id,
                                                                                   term_id=w)

        return

    def _norm_conditional_mutual_information(self, n_11, n_10, n_01, n_00, n_1_, n__1, n_0_, n__0, n,
                                             n_docs, probability_term):
        mutual_information = (n_11 / n) * self.log2((n * n_11), (n_1_ * n__1)) \
                             + (n_01 / n) * self.log2((n * n_01), (n_0_ * n__1)) \
                             + (n_10 / n) * self.log2((n * n_10), (n_1_ * n__0)) \
                             + (n_00 / n) * self.log2((n * n_00), (n_0_ * n__0))

        joint_prob_class_pres = self.probability(n__1, n_docs)
        joint_prob_class_abs = self.probability(n__0, n_docs)
        joint_prob_term_pres = self.probability(n_1_, n_docs)
        joint_prob_term_abs = self.probability(n_0_, n_docs)

        cond_entropy_class = self.conditional_entropy(joint_proba_absence=joint_prob_class_abs,
                                                      joint_proba_presence=joint_prob_class_pres,
                                                      proba_presence=probability_term[0])
        cond_entropy_term = self.conditional_entropy(joint_proba_absence=joint_prob_term_abs,
                                                     joint_proba_presence=joint_prob_term_pres,
                                                     proba_presence=probability_term[0])
        return self.normalized_mutual_infomation(mutual_information, cond_entropy_term, cond_entropy_class)

    def _gen_norm_cond_mutual_info(self, X, y, class_id, conditional_term_id, term_id):
        current_class = (y == class_id)
        conditional_term = X[:, conditional_term_id]
        bool_conditional_term = (conditional_term == 1) * 1  # set conditional_term = 1
        confusion_matrix = np.zeros((2, 2))
        current_term = X[:, term_id]

        bool_term_1 = (current_term != 0) * 1  # set term = 1
        bool_term_0 = (current_term == 0) * 1  # set term = 0
        bool_class_1 = (current_class == 1) * 1  # set class = 1
        bool_class_0 = (current_class == 0) * 1  # set class = 0

        sum_cond_term_and_class = np.sum(np.asarray([bool_conditional_term, bool_class_1]), axis=0)
        sum_all = np.sum(np.asarray([sum_cond_term_and_class, bool_term_1]), axis=0)
        confusion_matrix[0, 0] = np.count_nonzero(sum_all == 3)  # term = 1 | class = 1
        del sum_cond_term_and_class
        del sum_all

        sum_cond_term_and_class = np.sum(np.asarray([bool_conditional_term, bool_class_1]), axis=0)
        sum_all = np.sum(np.asarray([sum_cond_term_and_class, bool_term_0]), axis=0)
        confusion_matrix[1, 0] = np.count_nonzero(sum_all == 3)  # term = 0 | class = 1
        del sum_cond_term_and_class
        del sum_all

        sum_cond_term_and_class = np.sum(np.asarray([bool_conditional_term, bool_class_0]), axis=0)
        sum_all = np.sum(np.asarray([sum_cond_term_and_class, bool_term_0]), axis=0)
        confusion_matrix[1, 1] = np.count_nonzero(sum_all == 3)  # term = 0 | class = 0
        del sum_cond_term_and_class
        del sum_all

        sum_cond_term_and_class = np.sum(np.asarray([bool_conditional_term, bool_class_0]), axis=0)
        sum_all = np.sum(np.asarray([sum_cond_term_and_class, bool_term_1]), axis=0)
        confusion_matrix[0, 1] = np.count_nonzero(sum_all == 3)  # term = 1 | class = 0
        del sum_cond_term_and_class
        del sum_all

        if np.sum(confusion_matrix):
            return self._norm_conditional_mutual_information(n_11=confusion_matrix[0, 0],
                                                             n_10=confusion_matrix[0, 1],
                                                             n_01=confusion_matrix[1, 0],
                                                             n_00=confusion_matrix[1, 1],
                                                             n_1_=np.sum(confusion_matrix, axis=1)[0],
                                                             n__1=np.sum(confusion_matrix, axis=0)[0],
                                                             n_0_=np.sum(confusion_matrix, axis=1)[1],
                                                             n__0=np.sum(confusion_matrix, axis=0)[1],
                                                             n=np.sum(confusion_matrix),
                                                             n_docs=X.shape[0],
                                                             probability_term=self.probability_term[conditional_term_id]
                                                             )
        else:
            return 0.0
