import numpy as np
from sklearn.model_selection import StratifiedKFold
from src.main.python.steps.weighting_step import WeightingStep
from src.main.python.ml_methods.bayesiancv_svm import BayesSearchSvm
from src.main.python.ml_methods.bayesiancv_lightgbm import BayesSearchLightGbm
from src.main.python.evaluation.metric import Evaluation


class BayesianCv:
    def __init__(self,
                 n_splits=3,
                 instantiations=["cw", "cw-pos", "cw-tf_al", "cw-pos-tf_al"],
                 method='svm'):
        self.instantiations = instantiations
        self.n_splits = n_splits
        self.results = list()
        self.method = method

    def run(self, X, y, dataset_name, fold, path_to_save_embeddings, input_lexicons, scoring="f1_macro"):

        self.results = list()
        for instantiation in self.instantiations:
            try:
                weighting_step = WeightingStep()
                weighting_step.fit(training_docs=X,
                                   npz_path=path_to_save_embeddings,
                                   dataset_name=dataset_name,
                                   input_lexicons=input_lexicons,
                                   fold=fold,
                                   instantiation=instantiation)

                y_cw_train, x_cw_train = weighting_step.transform(X=X,
                                                                  y=y,
                                                                  idf=False)

                print(f"x_cw_train contains NaN: {np.isnan(x_cw_train.data).any()}")
                print(f"x_cw_test contains Inf: {np.isinf(x_cw_train.data).any()}")
          
                # x_cw_train.data = np.nan_to_num(x_cw_train.data, nan=.0, posinf=.0, neginf=.0, copy=False)
                #
                # print(f"x_cw_train contains NaN: {np.isnan(x_cw_train.data).any()}")
                # print(f"x_cw_test contains Inf: {np.isinf(x_cw_train.data).any()}")
                print(f"Instantiation: {instantiation}")

                bayesian_cv = None
                if self.method == 'svm':
                    print("SVM")
                    bayesian_cv = BayesSearchSvm(X_train=x_cw_train,
                                                 y_train=y_cw_train,
                                                 n_splits=self.n_splits,
                                                 scoring=scoring)
                elif self.method == 'light_gbm':
                    print("LightGBM")
                    bayesian_cv = BayesSearchLightGbm(X_train=x_cw_train,
                                                      y_train=y_cw_train,
                                                      n_splits=self.n_splits,
                                                      scoring=scoring)

                if bayesian_cv:
                    bayesian_cv.run()
                    self.results.append(bayesian_cv.get_best_score() * 100)

                del x_cw_train
                del y_cw_train
                del bayesian_cv
                del weighting_step


            except Exception as err:
                raise err

    def get_best_instantiation(self):
        if len(self.results) > 0:
            try:
                max_index = np.argmax(self.results)
                return self.instantiations[max_index]
            except Exception as err:
                message = f"Instantiations {self.instantiations}\n Results {self.results}\n Max: " \
                          f"{np.argmax(self.results)}"
                raise Exception(message)
        else:
            return None

    def log_instantiations(self):
        str = ""
        if len(self.results) > 0:
            for instantiation_index in range(len(self.instantiations)):
                str += f"{self.instantiations[instantiation_index]}\t{self.results[instantiation_index]}\n"

            max_index = np.argmax(self.results)
            str += f"Best Instantiation\n{self.instantiations[max_index]}\t{self.results[max_index]}\n"

            return str

        else:
            return ""
