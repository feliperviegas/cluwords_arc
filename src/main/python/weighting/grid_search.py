import numpy as np
from collections import Counter
from sklearn.model_selection import StratifiedKFold
from src.main.python.steps.weighting_step import WeightingStep
from src.main.python.ml_methods.classifiers import Classifier
from src.main.python.evaluation.metric import Evaluation

from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import normalize


class GridSearch:
    def __init__(self,
                 n_splits=3,
                 instantiations=["cw", "cw-pos", "cw-tf_al", "cw-pos-tf_al"],
                 with_compression=False,
                 svd_latent_dimension=100):
        self.instantiations = instantiations
        self.n_splits = n_splits
        self.results = list()
        self.with_compression = with_compression
        self.svd_latent_dimension = svd_latent_dimension

    # TODO - Create list for MacroF1 and MicroF1
    def run(self, X, y, dataset_name, fold, path_to_save_representation,
            path_to_save_embeddings, input_lexicons, metric="macro"):
        skf = StratifiedKFold(self.n_splits)
        skf.get_n_splits(X, y)
        self.results = list()
        for instantiation in self.instantiations:
            try:
                weighting_step = WeightingStep()
                weighting_step.fit(training_docs=X,
                                   npz_path=path_to_save_embeddings,
                                   dataset_name=dataset_name,
                                   input_lexicons=input_lexicons,
                                   fold=fold,
                                   instantiation=instantiation)

                y_cw_train, x_cw_train = weighting_step.transform(X=X,
                                                                  y=y,
                                                                  idf=False)
                if self.with_compression:
                    # Apply SVD
                    svd = TruncatedSVD(n_components=self.svd_latent_dimension, n_iter=500, random_state=42)
                    svd.fit(x_cw_train)
                    x_cw_train = svd.transform(x_cw_train)

                    # Apply l2 Norm
                    normalize(X=x_cw_train, norm='l2', axis=1, copy=False)

                svm = Classifier(name='svm', c_range=np.arange(-5, 15, 2))
                best_search_score = svm.search(x_train=x_cw_train,
                                               y_train=y_cw_train,
                                               cv=self.n_splits,
                                               n_jobs=4,
                                               n_iter=8)

                self.results.append(np.mean(best_search_score) * 100)


            except Exception as err:
                raise err

    def run_val(self, X_train, y_train, 
                X_val, y_val,
                dataset_name, fold, path_to_save_representation,
                path_to_save_embeddings, input_lexicons, metric="macro"):
        
        self.results = list()
        for instantiation in self.instantiations:
            try:
                weighting_step = WeightingStep()
                weighting_step.fit(training_docs=X_train,
                                   npz_path=path_to_save_embeddings,
                                   dataset_name=dataset_name,
                                   input_lexicons=input_lexicons,
                                   fold=fold,
                                   instantiation=instantiation)

                y_cw_train, x_cw_train = weighting_step.transform(X=X_train,
                                                                  y=y_train,
                                                                  idf=False)

                y_cw_val, x_cw_val = weighting_step.transform(X=X_val,
                                                              y=y_val,
                                                              idf=False)

                # print(f"Train: {x_cw_train.shape}")
                # print(f"Train: {Counter(y_cw_train)}")
                # print(f"Val: {x_cw_val.shape}")
                # print(f"Val: {Counter(y_cw_val)}")


                if self.with_compression:
                    # Apply SVD
                    svd = TruncatedSVD(n_components=self.svd_latent_dimension, n_iter=500, random_state=42)
                    svd.fit(x_cw_train)
                    x_cw_train = svd.transform(x_cw_train)

                    # Apply l2 Norm
                    normalize(X=x_cw_train, norm='l2', axis=1, copy=False)

                svm = Classifier(name='svm', c_range=np.arange(-5, 15, 2))
                svm_pred, _ = svm.run(x_train=x_cw_train,
                                                  y_train=y_cw_train,
                                                  x_test=x_cw_val,
                                                  y_test=y_cw_val,
                                                  fold=fold,
                                                  n_jobs=4,
                                                  verbose=False,
                                                  path_to_save=None)
                
                score = Evaluation().evaluate(true=y_cw_val, pred=svm_pred, type="macro")
                self.results.append(score * 100) 

            except Exception as err:
                raise err
        
        return 

    def get_best_instantiation(self):
        if len(self.results) > 0:
            try:
                max_index = np.argmax(self.results)
                return self.instantiations[max_index]
            except Exception as err:
                message = f"Instantiations {self.instantiations}\n Results {self.results}\n Max: " \
                          f"{np.argmax(self.results)}"
                raise Exception(message)
        else:
            return None

    def log_instantiations(self):
        str = ""
        if len(self.results) > 0:
            for instantiation_index in range(len(self.instantiations)):
                str += f"{self.instantiations[instantiation_index]}\t{self.results[instantiation_index]}\n"

            max_index = np.argmax(self.results)
            str += f"Best Instantiation\n{self.instantiations[max_index]}\t{self.results[max_index]}\n"

            return str

        else:
            return ""
