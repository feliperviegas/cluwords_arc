import shutil
import numpy as np
import scipy.sparse

def copy_file(source, destination):
    shutil.copy2(source, destination)
    return

def get_density(sparse_matrix):
    (x, y, z) = scipy.sparse.find(sparse_matrix)
    countings = np.bincount(x)
    density_documents = countings
    mean_density = np.mean(density_documents)
    max_density = np.max(density_documents)
    min_density = np.min(density_documents)
    return mean_density, min_density, max_density