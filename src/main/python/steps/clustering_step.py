from src.main.python.clustering.generate_semantic_information import GenerateSemanticInformation


class ClusteringStep:
    def __init__(self):
        self.threshold = None

    def run(self, word_vectors, vocabulary_embedding, path_to_save_results, n_threads,
            k, algorithm_type, fold, dataset, mode, threshold=None):
        obj = GenerateSemanticInformation(algorithm=algorithm_type,
                                    word_vectors=word_vectors,
                                    vocabulary_embedding=vocabulary_embedding,
                                    k_neighbors=k,
                                    threshold=threshold,
                                    n_jobs=n_threads,
                                    dataset_name=dataset,
                                    fold=fold,
                                    mode=mode,
                                    path_to_save_results=path_to_save_results)
        
        self.threshold = obj.get_threshold()

    def get_threshold(self):
        return self.threshold
