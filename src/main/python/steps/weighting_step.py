import timeit
import warnings
import spacy
import math
import numpy as np
from scipy import sparse
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.preprocessing import normalize
from sklearn.datasets import dump_svmlight_file
from src.main.python.structure.incremental_coo_matrix import IncrementalCOOMatrix

# from analysis import generate_density_filter_lexicon # TODO - Add this source code!

#TODO 
# https://numpy.org/doc/stable/reference/generated/numpy.einsum.html

NLP = spacy.load("en_core_web_sm")


class WeightingStep(object):
    """
    Description
    -----------
    Calculates Terme Frequency-Inverse Document Frequency (TFIDF) for cluwords.

    Parameters
    ----------
    dataset_file_path : str
        The complete dataset file path.
    n_words : int
        Number of words in the dataset.
    path_to_save_cluwords : list, default None
        Path to save the cluwords file.
    class_file_path: str, (default = None)
        The path to the file with the class of the dataset.

    Attributes
    ----------
    dataset_file_path: str
        The dataset file path passed as parameter.
    n_words: int
        Number of words passed as paramter.
    cluwords_tf_idf: ndarray
        Product between term frequency and inverse term frequency.
    cluwords_idf: str

    """

    def __init__(self, n_jobs=1, sublinear_tf=False):

        self.n_jobs = n_jobs
        self.sublinear_tf = sublinear_tf
        self.cluwords_tf_idf = None
        self.idf = None
        self.is_fit = 0
        self.hyp_sim = None
        self.training_docs = None
        self.n_documents = None
        self.n_words = -1  # Default value TODO - Check if it is different than -1
        self.vocab = None
        self.vocab_cluwords = None
        self.lexicon_words = None
        self.lexicons_pos_scores = None
        self.lexicons_neg_scores = None

    # Substiuido pela script read_documents
    # @staticmethod
    # def read_input(input_file):
    #     arq = open(input_file, 'r')
    #     doc = arq.readlines()
    #     arq.close()
    #     documents = list(map(str.rstrip, doc))
    #     n_documents = len(documents)
    #     return documents, n_documents

    # Substiuido pela script read_documents
    # def raw_tf(self, data, binary=False, dt=np.float32):
    #     tf_vectorizer = CountVectorizer(max_features=self.n_words, binary=binary, vocabulary=self.vocab)
    #     documents, n_documents = self.read_input(data)
    #     tf_vectorizer.fit(self.documents)
    #     tf = tf_vectorizer.transform(documents)
    #     return np.asarray(tf.toarray(), dtype=dt)

    @staticmethod
    def normalize(data, normalization_function='l2', has_test=False, train_size=0):
        n_rows = data.shape[0]
        normalize(data, norm=normalization_function,
                  axis=1, copy=False, return_norm=False)
        if has_test:
            train = np.take(data, np.arange(train_size), axis=0)
            test_size = n_rows - train_size
            test = np.take(data, (np.arange(test_size) + train_size), axis=0)
            return train, test
        else:
            return data

    # def fit(self, data, npz_path, dataset_name, class_id=0, fold=0, alpha=0.4):
    #     self.documents, self.n_documents = self.read_input(data)
    #
    #     try:
    #         loaded = np.load('{}/{}_cluwords_information_{}.npz'.format(npz_path,
    #                                                                     dataset_name,
    #                                                                     fold))
    #         self.vocab = loaded['words_vocab']
    #         self.vocab_cluwords = loaded['cluwords_vocab']
    #         self.n_words = len(self.vocab)
    #         del loaded
    #     except IOError:
    #         print("Error opening {}/{}_cluwords_information_{}.npz".format(npz_path,
    #                                                                        dataset_name,
    #                                                                        fold))
    #         exit(0)
    #
    #     # import pdb; pdb.set_trace();
    #     similarity_matrix = sparse.csr_matrix((self.n_words, self.n_words), dtype=np.float32)
    #     try:
    #         similarity_matrix = sparse.load_npz('{}/{}_cluwords_cosine_{}.npz'.format(npz_path,
    #                                                                                   dataset_name,
    #                                                                                   fold))
    #         print('Matrix{}'.format(similarity_matrix.shape))
    #     except IOError:
    #         print("Error opening cluwords_cosine.npz")
    #         exit(0)
    #
    #     print('Computing IDF...')
    #     # self.idf = self._idf(data=data,
    #     #                      similarity_matrix=similarity_matrix.toarray())
    #     self.idf = self._idf_sparse(data=data,
    #                                 similarity_matrix=similarity_matrix)
    #
    #     norm_cond_mutual_info = sparse.csr_matrix((self.n_words, self.n_words), dtype=np.float32)
    #     try:
    #         norm_cond_mutual_info = sparse.load_npz('{}/{}_cluwords_ncmi_{}_{}.npz'.format(npz_path, dataset_name,
    #                                                                                        class_id, fold))
    #         print('Matrix{}'.format(norm_cond_mutual_info.shape))
    #     except IOError:
    #         print("Error opening cluwords_ncmi.npz")
    #         exit(0)
    #
    #     similarity_matrix = np.multiply(similarity_matrix, alpha)
    #     norm_cond_mutual_info = np.multiply(norm_cond_mutual_info, 1.0 - alpha)
    #     hyp_sim = similarity_matrix + norm_cond_mutual_info
    #     self.hyp_sim = hyp_sim
    #     self.is_fit = True
    #     return

    # @staticmethod
    # def _get_all_scores(npz_path, n_classes, n_words, dataset_name, sim_matrix=None, fold=0):
    #     hyp_sim = None
    #     for class_id in range(n_classes):
    #         norm_cond_mutual_info = sparse.csr_matrix((n_words, n_words), dtype=np.float32)
    #         try:
    #             norm_cond_mutual_info = sparse.load_npz('{}/{}_cluwords_ncmi_{}_{}.npz'.format(npz_path, dataset_name,
    #                                                                                            class_id, fold))
    #             print('Matrix{}'.format(norm_cond_mutual_info.shape))
    #             if class_id != 0:
    #                 if sim_matrix:
    #                     sim_matrix = np.multiply(sim_matrix, alpha)
    #                     norm_cond_mutual_info = np.multiply(norm_cond_mutual_info, 1.0 - alpha)
    #                     hyp_sim = sparse.hstack((hyp_sim, (similarity_matrix + norm_cond_mutual_info)), format='csr')
    #                 else:
    #                     hyp_sim = sparse.vstack((hyp_sim, norm_cond_mutual_info), format='csr')
    #             else:
    #                 if sim_matrix:
    #                     hyp_sim = (similarity_matrix + norm_cond_mutual_info)
    #                 else:
    #                     hyp_sim = norm_cond_mutual_info.copy()
    #
    #         except Exception as err:
    #             raise err
    #
    #         del norm_cond_mutual_info
    #
    #     print(hyp_sim.shape)
    #     return hyp_sim

    def fit(self, training_docs, npz_path, dataset_name, instantiation="cw",
            input_lexicons=None, fold=0):
        self.training_docs = training_docs
        self.n_documents = self.training_docs.shape[0]

        if input_lexicons is not None and input_lexicons.find("senti_lexicons_") != -1:
            input_lexicons = input_lexicons + f"_{fold}.txt"

        try:
            loaded = np.load('{}/{}_cluwords_information_{}.npz'.format(npz_path,
                                                                        dataset_name,
                                                                        fold))
            self.vocab = loaded['index']
            self.vocab_cluwords = loaded['cluwords']
            self.n_words = len(self.vocab)
            del loaded
        except IOError:
            raise Exception("Error opening {}/{}_cluwords_information_{}.npz".format(npz_path,
                                                                           dataset_name,
                                                                           fold))

        similarity_matrix = sparse.csr_matrix((self.n_words, self.n_words), dtype=np.float32)
        try:
            similarity_matrix = sparse.load_npz('{}/{}_cluwords_cosine_{}.npz'.format(npz_path,
                                                                                      dataset_name,
                                                                                      fold))
            # print('Matrix{}'.format(similarity_matrix.shape))
        except IOError:
            raise Exception("Error opening cluwords_cosine.npz")

        if instantiation == "cw":
            self.hyp_sim = similarity_matrix
        elif instantiation == "cw-pos":
            self.hyp_sim = self.build_matrix_with_part_of_speech(similarity_matrix=similarity_matrix)
        elif instantiation == "cw-tf_al":
            if input_lexicons is not None:
                self.load_lexicons(input_lexicons=input_lexicons)
            else:
                raise Exception("Lexicons not found.")

            self.hyp_sim = self.build_lexicon_matrix_without_part_of_speech(similarity_matrix=similarity_matrix)
        elif instantiation == "cw-pos-tf_al":
            if input_lexicons is not None:
                self.load_lexicons(input_lexicons=input_lexicons)
            else:
                raise Exception("Lexicons not found.")

            self.hyp_sim = self.build_lexicon_matrix_with_part_of_speech(similarity_matrix=similarity_matrix)
        else:
            self.hyp_sim = similarity_matrix

        # print('Computing IDF...')
        self.idf = self._idf_sparse(similarity_matrix=self.hyp_sim)

        # print(f'IDF: {self.idf.shape}')
        self.is_fit = True
        return

    def transform(self, X, y, compacted=False, idf=True):
        if self.is_fit:
            # print('Computing TF-IDF...')
            y, tf_idf = self._tf(X=X, y=y)
            if idf:
                tf_idf = np.multiply(tf_idf, self.idf)

            if compacted:
                X_bin = self._gen_bin_matrix(X)
                tf_idf = np.multiply(tf_idf, X_bin)

            return y, tf_idf
        else:
            raise Exception("Must fit the model.")

    def _tf(self, X, y):
        # start = timeit.default_timer()
        warnings.simplefilter("error", RuntimeWarning)
        if self.sublinear_tf:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                X = 1. + np.log10(X)
                X[np.isneginf(X)] = 0

        # cluwords_tf = np.dot(tf, np.transpose(self.hyp_sim))
        # TODO - Check it is necessary the toarray in the next code line
        cluwords_tf = sparse.csr_matrix(X).dot(sparse.csr_matrix.transpose(self.hyp_sim)).toarray()
        # end = timeit.default_timer()
        # print("Cluwords TF done in %0.3fs." % (end - start))
        # print(f'TF {cluwords_tf.shape}')
        return y, cluwords_tf

    @staticmethod
    def _gen_bin_matrix(matrix):
        return sparse.csr_matrix((matrix > 0) * 1., dtype=np.float32)

    def _idf_sparse(self, similarity_matrix):
        # if self.smooth_neighbors:
        #     self.tfIdfTransformer = TfidfTransformer(norm=None, use_idf=True, smooth_idf=True)
        #     self.tfIdfTransformer.fit_transform(similarity_matrix)

        # start = timeit.default_timer()
        # print('Read data')
        # TODO ------ Remove later these code lines
        # tf = self.raw_tf(binary=True, dt=np.float32, data=data)
        # tf = sparse.csr_matrix(tf.copy())
        # TODO ------
        tf = self._gen_bin_matrix(matrix=self.training_docs)
        # end = timeit.default_timer()
        # print('Time {}'.format(end - start))
        start = timeit.default_timer()
        # print('Dot tf and hyp_aux')
        _dot = tf.dot(sparse.csr_matrix.transpose(similarity_matrix))  # np.array n_documents x n_cluwords
        # end = timeit.default_timer()
        # print('Time {}'.format(end - start))
        similarity_matrix_bin = self._gen_bin_matrix(matrix=similarity_matrix)
        # start = timeit.default_timer()
        # print('Dot tf and bin hyp_aux')
        _dot_bin = tf.dot(sparse.csr_matrix.transpose(similarity_matrix_bin))
        # end = timeit.default_timer()
        # print('Time {}'.format(end - start))
        # start = timeit.default_timer()
        # print('Divide _dot and _dot_bin')
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            mu_hyp = np.nan_to_num(np.divide(_dot, _dot_bin))

        # end = timeit.default_timer()
        # print('Time {}'.format(end - start))
        # start = timeit.default_timer()
        # print('Sum')
        cluwords_idf = np.sum(mu_hyp, axis=0)
        cluwords_idf[cluwords_idf == .0] = 0.001
        # end = timeit.default_timer()
        # print('Time {}'.format(end - start))
        # start = timeit.default_timer()
        # print('log')
        cluwords_idf = np.log10(np.divide(self.n_documents, cluwords_idf))
        # print('IDF shape {}'.format(cluwords_idf.shape))
        # end = timeit.default_timer()
        # print('Time {}'.format(end - start))
        return np.asarray(cluwords_idf)

    @staticmethod
    def save_tf_idf_features_libsvm(cluwords_x, cluwords_y, path_to_save_cluwords,
                                    file_name, class_id=0, is_binary=False):
        outpufile = path_to_save_cluwords + '/{}'.format(file_name)
        if is_binary:
            cluwords_y = (cluwords_y == class_id) * 1

        dump_svmlight_file(cluwords_x, cluwords_y, outpufile, zero_based=False)
        return

    # This function can be included inside fit method. Maybe later we add there!
    def load_lexicons(self, input_lexicons, binary=False):
        lexicon_words = dict()
        lexicon_scores = np.zeros(self.n_words, dtype=np.float32)
        if binary:
            with open(input_lexicons, encoding='utf8', errors='ignore') as lexicons:
                for lexicon in lexicons:
                    lexicon_word, sentiment_score = lexicon.strip().split('\t')[:2]
                    if lexicon_word in self.vocab:
                        lexicon_words[lexicon_word] = float(sentiment_score)
                        index = self.vocab.tolist().index(lexicon_word)
                        if float(sentiment_score) > 0:
                            lexicon_scores[index] = 1.
                        elif float(sentiment_score) < 0:
                            lexicon_scores[index] = -1.
                        else:
                            lexicon_scores[index] = .0

                lexicons.close()
        else:
            with open(input_lexicons, encoding='utf8', errors='ignore') as lexicons:
                for lexicon in lexicons:
                    lexicon_word, sentiment_score = lexicon.strip().split('\t')[:2]
                    if lexicon_word in self.vocab:
                        float_score = float(sentiment_score)
                        round_score = math.floor(float_score) if float_score > .0 else math.ceil(float_score)
                        lexicon_words[lexicon_word] = float_score
                        # lexicon_words[lexicon_word] = round_score
                        index = self.vocab.tolist().index(lexicon_word)
                        lexicon_scores[index] = float_score
                        # lexicon_scores[index] = round_score

                lexicons.close()

        # + .0 is to remove the -0. values
        lexicon_pos_scores = (lexicon_scores * ((lexicon_scores > .0) * 1.)) + .0

        lexicon_neg_scores = (lexicon_scores * ((lexicon_scores < .0) * -1.)) + .0

        self.lexicon_words = lexicon_words
        self.lexicons_pos_scores = lexicon_pos_scores
        self.lexicons_neg_scores = lexicon_neg_scores
        return

    def build_lexicon_matrix_with_part_of_speech(self, similarity_matrix):
        pos_voc = self.get_part_of_speech()

        # Filter polarity and weight by its intensity -> index zero
        word = self.vocab_cluwords[0]
        if word in self.lexicon_words.keys():
            sim_row = np.ravel(similarity_matrix[0].todense())
            if self.lexicon_words[word] > .0:
                result = sim_row * self.lexicons_pos_scores
            elif self.lexicon_words[word] < .0:
                result = sim_row * self.lexicons_neg_scores
            else:
                result = np.zeros(self.n_words, dtype=np.float32)
                result[0] = 1.
        else:
            result = np.zeros(self.n_words, dtype=np.float32)
            result[0] = 1.

        # Filter by Part-of-Speech
        for sim_index in range(0, len(result)):
            neigh_word = self.vocab[sim_index]
            try:
                if pos_voc[word] != pos_voc[neigh_word]:
                    result[sim_index] = .0
            except KeyError as err:
                # logger.warning(f"Warning {word} - {neigh_word}")
                break

        # Initializing the hyp_sim matrix - This is why the index zero is apart from the other indexes
        hyp_sim = sparse.csr_matrix([result], dtype=np.float32)

        # Filter polarity and weight by its intensity -> other indexes
        for index in range(1, len(self.vocab_cluwords)):
            word = self.vocab_cluwords[index]
            if word in self.lexicon_words.keys():
                sim_row = np.ravel(similarity_matrix[index].todense())
                if self.lexicon_words[word] > .0:
                    result = sim_row * self.lexicons_pos_scores

                elif self.lexicon_words[word] < .0:
                    result = sim_row * self.lexicons_neg_scores

                else:
                    result = np.zeros(self.n_words, dtype=np.float32)
                    result[index] = 1.
            else:
                result = np.zeros(self.n_words, dtype=np.float32)
                result[index] = 1.

            # Filter by Part-of-Speech
            for sim_index in range(0, len(result)):
                neigh_word = self.vocab[sim_index]
                try:
                    if pos_voc[word] != pos_voc[neigh_word]:
                        result[sim_index] = .0
                except KeyError as err:
                    # logger.warning(f"Warning {word} - {neigh_word}")
                    break

            hyp_sim = sparse.vstack((hyp_sim,
                                     sparse.csr_matrix([result], dtype=np.float32)),
                                    format='csr')

        return hyp_sim

    def build_lexicon_matrix_without_part_of_speech(self, similarity_matrix):
        pos_voc = self.get_part_of_speech()

        # Filter polarity and weight by its intensity -> index zero
        word = self.vocab_cluwords[0]
        if word in self.lexicon_words.keys():
            sim_row = np.ravel(similarity_matrix[0].todense())
            if self.lexicon_words[word] > .0:
                result = sim_row * self.lexicons_pos_scores
            elif self.lexicon_words[word] < .0:
                result = sim_row * self.lexicons_neg_scores
            else:
                result = np.zeros(self.n_words, dtype=np.float32)
                result[0] = 1.
        else:
            result = np.zeros(self.n_words, dtype=np.float32)
            result[0] = 1.

        # Initializing the hyp_sim matrix - This is why the index zero is apart from the other indexes
        hyp_sim = sparse.csr_matrix([result], dtype=np.float32)

        # Filter polarity and weight by its intensity -> other indexes
        for index in range(1, len(self.vocab_cluwords)):
            word = self.vocab_cluwords[index]
            if word in self.lexicon_words.keys():
                sim_row = np.ravel(similarity_matrix[index].todense())
                if self.lexicon_words[word] > .0:
                    result = sim_row * self.lexicons_pos_scores

                elif self.lexicon_words[word] < .0:
                    result = sim_row * self.lexicons_neg_scores

                else:
                    result = np.zeros(self.n_words, dtype=np.float32)
                    result[index] = 1.
            else:
                result = np.zeros(self.n_words, dtype=np.float32)
                result[index] = 1.

            hyp_sim = sparse.vstack((hyp_sim,
                                     sparse.csr_matrix([result], dtype=np.float32)),
                                    format='csr')

        return hyp_sim

    def build_matrix_with_part_of_speech(self, similarity_matrix):
        pos_voc = self.get_part_of_speech()

        # Filter part-of-speech -> index zero
        word = self.vocab_cluwords[0]
        result = np.ravel(similarity_matrix[0].todense())
        for sim_index in range(0, len(result)):
            neigh_word = self.vocab[sim_index]
            try:
                if pos_voc[word] != pos_voc[neigh_word]:
                    result[sim_index] = .0
            except KeyError as err:
                # logger.warning(f"Warning {word} - {neigh_word}")
                break

        # Initializing the hyp_sim matrix - This is why the index zero is apart from the other indexes
        hyp_sim = sparse.csr_matrix([result], dtype=np.float32)

        # Filter part-of-speech -> other indexes
        for index in range(1, len(self.vocab_cluwords)):
            word = self.vocab_cluwords[index]
            result = np.ravel(similarity_matrix[index].todense())
            for sim_index in range(0, len(result)):
                neigh_word = self.vocab[sim_index]
                try:
                    if pos_voc[word] != pos_voc[neigh_word]:
                        result[sim_index] = .0
                except KeyError as err:
                    # logger.warning(f"Warning {word} - {neigh_word}")
                    break

            hyp_sim = sparse.vstack((hyp_sim,
                                     sparse.csr_matrix([result], dtype=np.float32)),
                                    format='csr')

        return hyp_sim

    def get_part_of_speech(self):
        doc = NLP(' '.join(word for word in self.vocab))
        pos_vocab = {}
        for token in doc:
            pos_vocab[token.text] = token.pos_

        return pos_vocab
