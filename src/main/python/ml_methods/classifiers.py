import argparse
import numpy as np
import json
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.datasets import load_svmlight_file
from sklearn.utils import parallel_backend
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.calibration import CalibratedClassifierCV
from sklearn.metrics import confusion_matrix
from itertools import product


class LogisticRegressionMult(object):
    @staticmethod
    def run(x_train, y_train, x_test, y_test, n_jobs, class_id, fold, cv=5, log=False, path_to_save=None):
        parallel_backend('threading', n_jobs=n_jobs)
        clf = LogisticRegression(random_state=42, max_iter=5000)
        # parameters = {'penalty': ['l1', 'l2'], 'class_weight': ['balanced', None], 'solver': ['liblinear'],
        #               'C': np.logspace(-5, 3, 15)}
        parameters = {'penalty': ['l1', 'l2'], 'class_weight': ['balanced'], 'solver': ['liblinear'],
                      'C': np.logspace(-5, 3, 15)}
        clf = GridSearchCV(clf, parameters, cv=cv, n_jobs=n_jobs, scoring='f1_macro', iid=True, verbose=True)
        clf.fit(x_train, y_train)
        if log:
            log_info = {
                'classifier': 'LogisticRegression',
                'random_state': 42,
                'n_jobs': n_jobs,
                'penalty': clf.best_params_['penalty'],
                'class_weight': clf.best_params_['class_weight'],
                'solver': clf.best_params_['solver'],
                'C': clf.best_params_['C'],
                'max_iter': 5000,
                'f1_score': clf.best_score_}

            with open('{}/log_file{}_{}'.format(path_to_save, class_id, fold), 'w') as output_file:
                json.dump(log_info, output_file)
                output_file.close()

        clf = LogisticRegression(random_state=42,
                                 n_jobs=n_jobs,
                                 penalty=clf.best_params_['penalty'],
                                 class_weight=clf.best_params_['class_weight'],
                                 solver=clf.best_params_['solver'],
                                 C=clf.best_params_['C'],
                                 max_iter=5000)
        clf.fit(x_train, y_train)
        y_pred = clf.predict(x_test)
        confusion = confusion_matrix(y_test, y_pred)
        y_predict_proba = clf.predict_proba(x_test)
        y_proba = []
        y_label = []
        for d in range(0, len(y_test)):
            y_label.append(int(class_id))
            y_proba.append(y_predict_proba[d][int(class_id)])

        return np.array([y_label], dtype=np.int16).T, np.array([y_proba], dtype=np.float32).T, confusion

    @staticmethod
    def write_output(filename_input, documents_label, y_proba, y_pred, fold):
        with open(filename_input, 'a') as regression_output:
            regression_output.write('#{}\n'.format(fold))
            count_classes = {}
            for d in range(0, len(documents_label)):
                count_classes[d] = []
                sorted_proba = np.argsort(y_proba[d])[::-1]
                regression_output.write('{} CLASS={}'.format(d, int(documents_label[d])))
                for sorted_index in sorted_proba:
                    count_classes[d].append(y_pred[d][sorted_index])
                    regression_output.write(' CLASS={}:{}'.format(y_pred[d][sorted_index], y_proba[d][sorted_index]))

                regression_output.write('\n')

            regression_output.close()

        return


class LogisticRegressionSingleRepresentation(object):
    @staticmethod
    def run(x_train, y_train, x_test, y_test, fold, n_jobs=-1, cv=5, log=False, path_to_save=None, full_grid=False):
        parallel_backend('threading', n_jobs=n_jobs)
        clf = LogisticRegression(random_state=42)
        if full_grid:
            penalty = ['l1', 'l2', 'elasticnet']
            class_weight = ['balanced', None]
            solver = ['lbfgs', 'newton-cg', 'liblinear', 'sag', 'saga']
            C = np.logspace(-5, 3, 15)
            max_iter = [100, 1000, 2500, 5000]
            all_combinations = list(product(penalty, class_weight, solver, C, max_iter))
            parameters = [{'penalty': [penalty],
                           'solver': [solver],
                           'C': [C],
                           'max_iter': [max_iter]}
                          for penalty, class_weight, solver, C, max_iter in all_combinations
                          if not ((penalty == 'l1') and (solver == 'lbfgs'
                                                         or solver == 'saga'
                                                         or solver == 'sag'
                                                         or solver == 'newton-cg'))
                          and not (penalty == 'elasticnet' and (solver == 'lbfgs' or solver == 'liblinear'
                                                                or solver == 'sag' or solver == 'newton-cg'))]
        else:
            penalty = ['l2']
            class_weight = ['balanced']
            solver = ['liblinear']
            C = np.logspace(-5, 3, 15)
            max_iter = [2500]
            all_combinations = list(product(penalty, class_weight, solver, C, max_iter))
            parameters = [{'penalty': [penalty],
                           'class_weight': [class_weight],
                           'solver': [solver],
                           'C': [C],
                           'max_iter': [max_iter]}
                          for penalty, class_weight, solver, C, max_iter in all_combinations]

        # scoring = 'f1_macro'
        clf = GridSearchCV(clf, parameters, cv=cv, n_jobs=n_jobs, scoring='f1_micro', iid=True, verbose=True)
        clf.fit(x_train, y_train)

        if log:
            log_info = {
                'classifier': 'LogisticRegression',
                'random_state': 42,
                'n_jobs': n_jobs,
                'penalty': clf.best_params_['penalty'],
                'class_weight': clf.best_params_['class_weight'],
                'solver': clf.best_params_['solver'],
                'C': clf.best_params_['C'],
                'max_iter': clf.best_params_['max_iter'],
                'f1_score': clf.best_score_}

            with open('{}/log_regression_file_{}'.format(path_to_save, fold), 'w') as output_file:
                json.dump(log_info, output_file)
                output_file.close()

        clf = LogisticRegression(random_state=42,
                                 n_jobs=n_jobs,
                                 penalty=clf.best_params_['penalty'],
                                 class_weight=clf.best_params_['class_weight'],
                                 solver=clf.best_params_['solver'],
                                 C=clf.best_params_['C'],
                                 max_iter=clf.best_params_['max_iter'])
        clf.fit(x_train, y_train)
        y_pred = clf.predict(x_test)
        # y_decision_function = clf.decision_function(x_test)
        y_predict_proba = clf.predict_proba(x_test)
        confusion = confusion_matrix(y_test, y_pred)
        return y_pred, y_predict_proba, confusion

    @staticmethod
    def write_output(filename_input, documents_label, y_proba, y_pred, fold):
        with open(filename_input, 'a') as regression_output:
            regression_output.write('#{}\n'.format(fold))
            for d in range(0, len(documents_label)):
                regression_output.write('{} CLASS={} CLASS={}:{}\n'.format(
                    d + 1, int(documents_label[d]), int(y_pred[d]), y_proba[d][int(y_pred[d])]))

            regression_output.close()

        return


class Classifier:
    # TODO - Test -> loguniform(1e0, 1e3)
    # from sklearn.utils.fixes import loguniform
    def __init__(self, name, c_range=np.arange(-6, 15, 2)):
        self.name = name
        self.c_range = c_range
        self.parameters = self.select_parameters()
        self.estimator = self.select_estimator()

    def select_parameters(self):
        if self.name == 'svm':
            penalty = ['l2']
            loss = ['squared_hinge']
            dual = [False, True]
            C = 2.0 ** self.c_range
            max_iter = [2500]
            all_combinations = list(product(penalty, loss, dual, C, max_iter))
            parameters = [{'penalty': [penalty],
                           'loss': [loss],
                           'dual': [dual],
                           'C': [C],
                           'max_iter': [max_iter]}
                          for penalty, loss, dual, C, max_iter in all_combinations]
            return parameters
        elif self.name == 'knn':
            algorithm = ['auto']
            metric = ['cosine']
            n_neighbors = [10, 20, 30, 40, 50]
            all_combinations = list(product(algorithm, metric, n_neighbors))
            parameters = [{'algorithm': [algorithm],
                           'metric': [metric],
                           'n_neighbors': [n_neighbors],
                           }
                          for algorithm, metric, n_neighbors in all_combinations]
            return parameters
        elif self.name == 'logistic_regression':
            return {'C': 2.0 ** np.arange(-5, 15, 2)}
        elif self.name == 'passive_aggressive':
            return {'C': 2.0 ** np.arange(-5, 15, 2)}

    def select_estimator(self):
        if self.name == 'svm':
            return LinearSVC(tol=0.0001, multi_class='ovr', fit_intercept=True,
                             intercept_scaling=1, class_weight=None, verbose=0,
                             random_state=42, max_iter=5000)
        elif self.name == 'knn':
            return KNeighborsClassifier(algorithm='auto', metric='cosine')
        elif self.name == 'logistic_regression':
            return LogisticRegression(random_state=42)
        elif self.name == 'passive_aggressive':
            return PassiveAggressiveClassifier(random_state=42, max_iter=5000)

    def search(self, x_train, y_train, n_jobs=-1, cv=5, n_iter=12):
        # clf = GridSearchCV(self.estimator, self.parameters, cv=cv, n_jobs=n_jobs,
        #                    scoring=['f1_macro', 'f1_micro'], refit='f1_macro')

        clf = RandomizedSearchCV(estimator=self.estimator,
                                 param_distributions=self.parameters,
                                 cv=cv,
                                 n_jobs=n_jobs,
                                 random_state=42,
                                 scoring=['f1_macro', 'f1_micro'],
                                 refit='f1_macro',
                                 n_iter=n_iter)

        clf.fit(x_train, y_train)
        best_search_score = clf.best_score_

        return best_search_score

    def run(self, x_train, y_train, x_test, y_test, fold, path_to_save,
            n_jobs=-1, cv=5, verbose=False, n_iter=12):
        # clf = GridSearchCV(self.estimator, self.parameters, cv=cv, n_jobs=n_jobs,
        #                    scoring=['f1_macro', 'f1_micro'], refit='f1_macro')

        clf = RandomizedSearchCV(estimator=self.estimator,
                                 param_distributions=self.parameters,
                                 cv=cv,
                                 n_jobs=n_jobs,
                                 random_state=42,
                                 scoring=['f1_macro', 'f1_micro'],
                                 refit='f1_macro',
                                 n_iter=n_iter)

        clf.fit(x_train, y_train)

        if verbose:
            with open(f'{path_to_save}/log_{self.name}_file{fold}', 'w') as output_file:
                json.dump(clf.best_params_, output_file)
                output_file.write('\n')
                output_file.close()

        y_pred = clf.predict(x_test)
        # TODO - Maybe remove in the next release
        # confusion = confusion_matrix(y_test, y_pred)
        csvm = CalibratedClassifierCV(clf).fit(x_train, y_train)
        y_predict_proba = csvm.predict_proba(x_test)

        return y_pred, y_predict_proba

    @staticmethod
    def write_output(filename_input, documents_label, y_proba, y_pred, fold):
        if fold == 0:
            with open(filename_input, 'w') as classifier_output:
                classifier_output.close()

        with open(filename_input, 'a') as classifier_output:
            classifier_output.write('#{}\n'.format(fold))
            for d in range(0, len(documents_label)):
                classifier_output.write('{} CLASS={} CLASS={}:{}\n'.format(
                    d + 1, int(documents_label[d]), int(y_pred[d]), y_proba[d][int(y_pred[d])]))

            classifier_output.close()

        return


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset',
                        action='store',
                        type=str,
                        dest='dataset',
                        required=True,
                        help='-d [dataset folder name]')
    parser.add_argument('-c', '--classifier',
                        action='store',
                        type=str,
                        dest='classifier',
                        required=True,
                        help='-c [classifier name]')
    parser.add_argument('-f', '--fold',
                        action='store',
                        type=int,
                        required=True,
                        dest='fold',
                        help='--fold [TRAIN/TEST FOLD]')
    parser.add_argument('-o', '--output',
                        action='store',
                        type=str,
                        required=True,
                        dest='output',
                        help='--output [output path]')
    parser.add_argument('-tr', '--train',
                        action='store',
                        type=str,
                        dest='train',
                        required=True,
                        default='',
                        help='-tr [training set]')
    parser.add_argument('-te', '--test',
                        action='store',
                        type=str,
                        dest='test',
                        required=True,
                        default='',
                        help='-te [test set]')
    args = parser.parse_args()

    x_train, y_train = load_svmlight_file(args.train)
    x_train = x_train.todense()

    x_test, y_test = load_svmlight_file(args.test, n_features=x_train.shape[1])
    x_test = x_test.todense()

    filename_output = '{}/res_{}_pos_{}.txt'.format(args.output,
                                                    args.classifier,
                                                    args.dataset)

    # #Creating an empty file
    if args.fold == 0:
        with open(filename_output, 'w') as file_output:
            file_output.close()

    clas = Classifier(name=args.classifier)
    pred_, predict_proba_, confusion = clas.run(x_train=x_train,
                                                y_train=y_train,
                                                x_test=x_test,
                                                y_test=y_test,
                                                fold=args.fold,
                                                path_to_save=args.output)

    clas.write_output(filename_input=filename_output,
                      documents_label=y_test,
                      y_proba=predict_proba_,
                      y_pred=pred_,
                      fold=args.fold)


if __name__ == "__main__":
    main()
