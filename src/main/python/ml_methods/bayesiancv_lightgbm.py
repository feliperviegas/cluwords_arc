import numpy as np
from skopt import BayesSearchCV
from skopt.space import Real, Categorical, Integer
from skopt.plots import plot_objective, plot_histogram

from sklearn.datasets import load_digits
from lightgbm import LGBMClassifier

from sklearn.model_selection import StratifiedKFold

class BayesSearchLightGbm:
    def __init__(self, X_train, y_train, n_splits=3, scoring='f1_macro'):
        self.X_train = X_train
        self.y_train = y_train
        self.opt = None
        self.n_splits = n_splits
        self.scoring = scoring

    def run(self):
        skf = StratifiedKFold(n_splits=3,
                              shuffle=True,
                              random_state=22)

        lgb_search = {
            'n_estimators': Integer(30, 100, prior='uniform'),
            'learning_rate': Real(0.01, 1.0, prior='log-uniform'),
            'num_leaves': Integer(24, 80, prior='uniform'),
            'feature_fraction': Real(0.1, 0.9, prior='log-uniform'),
            'bagging_fraction': Real(0.8, 1, prior='uniform'),
            'max_depth': Integer(5, 30, prior='uniform'),
            'max_bin': Integer(20, 90, prior='uniform'),
            'min_data_in_leaf': Integer(20, 80, prior='uniform'),
            'min_sum_hessian_in_leaf': Integer(0, 100, prior='uniform'),
            'subsample': Real(0.01, 1.0, prior='uniform')
        }

        self.opt = BayesSearchCV(
            estimator=LGBMClassifier(random_state=8),
            # (parameter space, # of evaluations)
            search_spaces=[(lgb_search, 60)],
            cv=skf,
            return_train_score=False,
            n_iter=40,
            scoring='f1_macro',
            refit=True
        )

        self.opt.fit(self.X_train, self.y_train)

    def get_best_score(self):
        if self.opt:
            return self.opt.best_score_

    def get_best_params(self):
        if self.opt:
            return self.opt.best_params_

    def get_score(self, X_test, y_test):
        if self.opt:
            return self.opt.score(X_test, y_test)
