from skopt import BayesSearchCV
from skopt.space import Real, Categorical, Integer
from skopt.plots import plot_objective, plot_histogram

from sklearn.datasets import load_digits
from sklearn.svm import LinearSVC, SVC
from sklearn.pipeline import Pipeline


class BayesSearchSvm:
    def __init__(self, X_train, y_train, n_splits=3, scoring='f1_macro', n_jobs=9, n_points=9):
        self.X_train = X_train
        self.y_train = y_train
        self.opt = None
        self.n_splits = n_splits
        self.scoring = scoring
        self.n_jobs = n_jobs
        self.n_points = n_points

    def run(self):
        # explicit dimension classes can be specified like this
        svc_search = {
            'C': Real(1e-6, 1e+6, prior='log-uniform'),
            'gamma': Real(1e-6, 1e+1, prior='log-uniform'),
            'degree': Integer(1, 8),
            'kernel': Categorical(['linear', 'poly', 'rbf']),
            'shrinking': Categorical([True, False])
        }

        self.opt = BayesSearchCV(
            estimator=SVC(),
            # (parameter space, # of evaluations)
            search_spaces=[(svc_search, 40)],
            cv=self.n_splits,
            random_state=8,
            scoring=self.scoring,
            refit=True,
            n_jobs=self.n_jobs,
            n_points=self.n_points
        )

        self.opt.fit(self.X_train, self.y_train)

    def get_best_score(self):
        if self.opt:
            return self.opt.best_score_

    def get_best_params(self):
        if self.opt:
            return self.opt.best_params_

    def get_score(self, X_test, y_test):
        if self.opt:
            return self.opt.score(X_test, y_test)
