import argparse
import numpy as np
from sklearn.metrics import f1_score

class Evaluation(object):
    @staticmethod
    def evaluate(true: list, pred: list, type: str) -> float:
        return f1_score(true, pred, average=type)

    def read_path(self, path: str, type: str) -> list:
        results = []
        with open(path, 'r') as file:
            for line in file:
                if line.strip() == '#0':
                    true = []
                    pred = []
                elif line[0] == '#':
                    results.append(self.evaluate(true=true, pred=pred, type=type))
                    true = []
                    pred = []
                else:
                    true_elem = line.strip().split(' ')[1].split('=')[1]
                    true.append(int(true_elem))
                    pred_elem = line.strip().split(' ')[2].split(':')[0].split('=')[1]
                    pred.append(int(pred_elem))

            results.append(self.evaluate(true=true, pred=pred, type=type))
            return results

    def get_mean_score(self, path: str, type: str) -> float:
        results = self.read_path(path=path, type=type)
        return np.mean(results)*100

    def get_mean_macro_micro_score(self, path: str) -> tuple:
        macro_results = self.read_path(path=path, type='macro')
        micro_results = self.read_path(path=path, type='micro')
        return round(np.mean(macro_results)*100, 2), round(np.mean(micro_results)*100, 2)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path',
                        action='store',
                        type=str,
                        dest='path',
                        required=True,
                        help='-d [dataset folder name]')
    parser.add_argument('-t', '--type',
                        action='store',
                        type=str,
                        dest='type',
                        required=True,
                        help='-t [macro or micro]')
    parser.add_argument('-s', '--strategy',
                        action='store',
                        type=str,
                        dest='strategy',
                        required=False,
                        help='-s [strategy name]')
    parser.add_argument('-m', '--mean',
                        action='store',
                        type=int,
                        dest='mean',
                        default=1,
                        required=False,
                        help='-m [mean option]')
    args = parser.parse_args()
    results = Evaluation().read_path(path=args.path, type=args.type)

    if args.mean:
        print(f'${round(np.mean(results)*100, 2)} {round(np.std(results, ddof=1)*100, 2)}$')
    else:
        for result in results:
            print(f'{args.strategy},{result*100}')


if __name__ == '__main__':
    main()
