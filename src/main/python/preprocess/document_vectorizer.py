import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

class DocumentVectorizer:
    def __init__(self):
        self.tf_vectorizer = None

    @staticmethod
    def read_raw_data(document_path):
        arq = open(document_path, 'r', encoding="utf-8")
        doc = arq.readlines()
        arq.close()
        documents = list(map(str.rstrip, doc))
        return documents

    def fit(self, documents, vocabulary):
        self.tf_vectorizer = CountVectorizer(max_features=len(vocabulary), vocabulary=vocabulary)
        self.tf_vectorizer.fit(documents)

    def transform(self, documents, dt=np.float32):
        if self.tf_vectorizer is not None:
            tf = self.tf_vectorizer.transform(documents)
            return np.asarray(tf.toarray(), dtype=dt)
        else:
            print("Not found Vectorizer object.")

    @staticmethod
    def get_binary(array):
        return (array > 0) * 1.

    @staticmethod
    def check_for_negative_values(array):
        sum_values = np.sum((array < 0) * 1.)
        if sum_values == 0:
            print("Documents have only positive values.")
        elif sum_values > 0:
            print("Documents have negative values.")
        else:
            print("Not sure what happened.")

    @staticmethod
    def as_numpy_array(list_data, dtype=np.int32):
        return np.asarray(list_data, dtype=dtype)