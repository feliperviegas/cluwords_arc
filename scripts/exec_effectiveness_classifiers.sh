#!/bin/bash

path=$1

#for dataset in  aisopos_ntua debate english_dailabor nikolaos_ted pang_movie \
#    sanders sarcasm sentistrength_bbc sentistrength_digg sentistrength_myspace \
#    sentistrength_rw sentistrength_twitter sentistrength_youtube stanford_tweets \
#    tweet_semevaltest vader_amazon vader_movie vader_nyt vader_twitter yelp_reviews ; do
for dataset in aisopos_ntua; do

  echo -n "${dataset} ";

#  for classifier in svm knn logistic_regression; do
#
#    for metric in macro micro; do

  for classifier in svm; do
      for metric in macro; do

        result=$(python3 ../src/main/python/evaluation/metric.py -p "${path}/datasets/${dataset}/res_${classifier}_${dataset}.txt" -t "${metric}");

      echo -n "& ${result} ";

    done

  done

  echo "\\\\ \\hline";

done
