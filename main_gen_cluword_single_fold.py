import os
from os import path
import argparse
import numpy as np
import logging
import logging.config
import timeit
from scipy import sparse
from src.main.python.embedding.create_fasttext_embedding import FastTextEmbedding
from src.main.python.preprocess.parse_splits import ParseRaw
from src.main.python.preprocess.document_vectorizer import DocumentVectorizer
from src.main.python.steps.clustering_step import ClusteringStep
from setting_env import EnvVariables
from src.main.python.steps.weighting_step import WeightingStep
from src.main.python.evaluation.metric import Evaluation
from src.main.python.embedding.bert_embedding import BertEmbedding

class InputParameters(object):
    def __init__(self, dataset_name, texts, labels, splits, fold, embedding, embedding_dimension, lexicons):
        self.dataset_name = dataset_name
        self.texts = texts
        self.labels = labels
        self.splits = splits
        self.fold = fold
        self.embedding = embedding
        self.embedding_dimension = embedding_dimension
        self.lexicons = lexicons

    def get_dataset_name(self):
        return self.dataset_name

    def get_texts(self):
        return self.texts

    def get_labels(self):
        return self.labels

    def get_splits(self):
        return self.splits

    def get_fold(self):
        return self.fold

    def get_embedding(self):
        return self.embedding

    def get_embedding_dimension(self):
        return self.embedding_dimension

    def get_lexicons(self):
        return self.lexicons


def main():
    method = 'light_gbm'
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset_name',
                        action='store',
                        type=str,
                        dest='dataset_name',
                        required=True,
                        help='-d [dataset folder name]')
    parser.add_argument('-t', '--texts',
                        action='store',
                        type=str,
                        dest='texts',
                        help='-t [texts folder name]')
    parser.add_argument('-l', '--labels',
                        action='store',
                        type=str,
                        dest='labels',
                        help='-l [labels folder name]')
    parser.add_argument('-e', '--embedding',
                        action='store',
                        type=str,
                        dest='embedding',
                        default=None,
                        help='-e [embedding file name]')
    parser.add_argument('-ed', '--embedding_dimension',
                        action='store',
                        type=int,
                        dest='embedding_dimension',
                        default=None,
                        help='-ed [embedding path]')
    parser.add_argument('-s', '--split',
                        action='store',
                        type=str,
                        default='',
                        dest='split',
                        help='-s [splits folder name]')
    parser.add_argument('-f', '--fold',
                        action='store',
                        type=int,
                        dest='fold',
                        required=True,
                        help='--fold [TRAIN/TEST FOLD]')
    parser.add_argument('-le', '--lexicons',
                        action='store',
                        type=str,
                        dest='lexicons',
                        required=False,
                        default=None,
                        help='--lexicons [lexicons]')
    args = parser.parse_args()

    env_variables = EnvVariables()
    input_params = InputParameters(dataset_name=args.dataset_name,
                                   texts=args.texts,
                                   labels=args.labels,
                                   splits=args.split,
                                   fold=args.fold,
                                   embedding=args.embedding,
                                   embedding_dimension=args.embedding_dimension,
                                   lexicons=args.lexicons)

    path_to_save_embeddings = f"{env_variables.get_path_to_save_embeddings()}/{input_params.get_dataset_name()}"
    path_to_save_representation = f"{env_variables.get_path_to_save_datasets()}/{input_params.get_dataset_name()}"
    # Create paths to save the experiments
    os.makedirs(f"{path_to_save_embeddings}", exist_ok=True)
    os.makedirs(f"{path_to_save_representation}", exist_ok=True)
    os.makedirs(f"/cluwords/tmp/", exist_ok=True)

    logging.config.fileConfig('logging.conf')

    # create logger
    logger = logging.getLogger(__name__)

    # #Create the word2vec models for each dataset
    logger.info("Filter embedding space to {} dataset.".format(input_params.get_dataset_name()))

    dataset_path = None
    logger.info(f"Voc type {env_variables.get_vocab_type()}.")
    logger.info(f"Mode {env_variables.get_mode()}.")
    try:
        if env_variables.get_vocab_type() == 0:
            logger.info('Vocabulary considered in the Embedding Filter Training set.')
            dataset_path = '{path}/d_train_data_{fold}.txt'.format(path=path_to_save_representation,
                                                                   fold=input_params.get_fold())
        elif env_variables.get_vocab_type() == 1:
            logger.info('Vocabulary considered in the Embedding Filter Collection.')
            dataset_path = input_params.get_texts()
        else:
            raise NameError("Setting not found. Check the .env file for VOCAB_TYPE variable.")

        if env_variables.get_mode() == "sparse" or env_variables.get_mode() == "memmap":
            logger.info("Mode loaded.")
        else:
            raise NameError("Setting not found. Structure Mode not found, check the .env file for MODE variable.")
    except NameError:
        raise

    word_vectors = None
    vocabulary_embedding = None
    if dataset_path is input_params.get_texts():
        start = timeit.default_timer()
        fasttext_embedding = FastTextEmbedding(embedding_dimension=input_params.get_embedding_dimension(),
                                               embedding_file_path=input_params.get_embedding(),
                                               embedding_type=env_variables.get_embedding_type(),
                                               dataset_path=dataset_path,
                                               path_to_save_model=path_to_save_representation)

        n_words = fasttext_embedding.get_n_words()
        word_vectors, vocabulary_embedding = fasttext_embedding.get_word_vectors()
        end = timeit.default_timer()
        logger.info(f"Loaded Embedding considering the vocabulary of the collection {end - start}.")

    macro_f1 = list()
    micro_f1 = list()

    fold = args.fold

    start = timeit.default_timer()
    ParseRaw(split_file=args.split,
             document_file=args.texts,
             label_file=args.labels,
             fold=fold,
             save_path=path_to_save_representation).run(apply_preprocess=False)
    end = timeit.default_timer()
    logger.info(f"Loaded train/test sets fold ({fold}) {end - start}.")

    if dataset_path is not input_params.get_texts():
        start = timeit.default_timer()
        try:
            bert_embedding = BertEmbedding(dataset_path=dataset_path,
                                           path_to_save_model=path_to_save_representation)

            n_words = bert_embedding.get_n_words()
            word_vectors, vocabulary_embedding = bert_embedding.get_word_vectors()
            end = timeit.default_timer()
            logger.info(f"Generated BERT Embedding considering the vocabulary of the training set {end - start}.")

        except Exception as err:
            raise err

    try:
        if word_vectors is None:
            raise NameError("Not found word_vectors variable.")

        if vocabulary_embedding is None:
            raise NameError("Not found vocabulary_embedding variable.")

    except NameError:
        raise

    logger.info(f'Number of words (n_words) {n_words}.')
    nearest_neighbors = 500 if n_words > 500 else n_words

    logger = logging.getLogger("ClusteringStep")
    start = timeit.default_timer()
    try:
        ClusteringStep().run(path_to_save_results=path_to_save_embeddings,
                             n_threads=env_variables.get_n_jobs(),
                             k=nearest_neighbors,
                             threshold=0.4,
                             algorithm_type=env_variables.get_clustering_method(),
                             # fold=input_params.get_fold(),
                             fold=fold,
                             dataset=input_params.get_dataset_name(),
                             mode=env_variables.get_mode(),
                             word_vectors=word_vectors,
                             vocabulary_embedding=vocabulary_embedding)
    except Exception as err:
        raise err

    end = timeit.default_timer()
    logger.info(f"Clustering step ({env_variables.get_clustering_method()})  {end - start}.")

    start = timeit.default_timer()

    # Vectorizing Traning Set and Test Set
    logger = logging.getLogger("VectorizerStep")
    vectorizer = DocumentVectorizer()

    training_docs = vectorizer.read_raw_data(
        document_path="{path}/d_train_data_{fold}.txt".format(path=path_to_save_representation,
                                                              fold=fold),
    )
    training_labels = vectorizer.read_raw_data(
        document_path="{path}/c_train_data_{fold}.txt".format(path=path_to_save_representation,
                                                              fold=fold),
    )
    y_train = vectorizer.as_numpy_array(training_labels)
    vectorizer.fit(documents=training_docs, vocabulary=vocabulary_embedding)
    X_train = vectorizer.transform(documents=training_docs)
    del training_docs
    del training_labels

    test_docs = vectorizer.read_raw_data(
        document_path="{path}/d_test_data_{fold}.txt".format(path=path_to_save_representation,
                                                             fold=fold),
    )
    test_labels = vectorizer.read_raw_data(
        document_path="{path}/c_test_data_{fold}.txt".format(path=path_to_save_representation,
                                                             fold=fold),
    )
    y_test = vectorizer.as_numpy_array(test_labels)
    X_test = vectorizer.transform(documents=test_docs)

    del test_docs
    del test_labels
    del vectorizer

    end = timeit.default_timer()
    logger.info(f"Vectorizer train/test sets {end - start}.")

    chosen_instantiation = env_variables.get_cluwords_type()
    logger.info(f"Instantiation {chosen_instantiation}")

    logger = logging.getLogger("WeightingStep")
    start = timeit.default_timer()
    try:
        weighting_step = WeightingStep()
        weighting_step.fit(training_docs=X_train,
                           npz_path=path_to_save_embeddings,
                           dataset_name=input_params.get_dataset_name(),
                           input_lexicons=input_params.get_lexicons(),
                           fold=fold,
                           instantiation=chosen_instantiation)

        y_cw_train, x_cw_train = weighting_step.transform(X=X_train,
                                                          y=y_train,
                                                          idf=False)
        del X_train
        del y_train

        np.savez_compressed("{}/cluwords_X_train_{}.npz".format(path_to_save_representation,
                                                                fold),
                            y=y_cw_train)
        sparse.save_npz("{}/cluwords_y_train_{}.npz".format(path_to_save_representation,
                                                            fold), sparse.csr_matrix(x_cw_train))
        del x_cw_train
        del y_cw_train

        y_cw_test, x_cw_test = weighting_step.transform(X=X_test,
                                                        y=y_test,
                                                        idf=False)
        del X_test
        del y_test

        np.savez_compressed("{}/cluwords_y_test_{}.npz".format(path_to_save_representation,
                                                                fold),
                            y=y_cw_test)
        sparse.save_npz("{}/cluwords_X_test_{}.npz".format(path_to_save_representation,
                                                           fold), sparse.csr_matrix(x_cw_test))
        del x_cw_test
        del y_cw_test
    except Exception as err:
        raise err

    end = timeit.default_timer()
    logger.info(f"Weighting step for the chosen instantiation {end - start}.")


if __name__ == '__main__':
    main()
