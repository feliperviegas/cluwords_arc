wget https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh

bash Anaconda3-2020.02-Linux-x86_64.sh

source ~/.bashrc 

conda create --name keras_cpu tensorflow python=3.6

conda activate keras_cpu

conda install -c conda-forge scikit-learn

conda install -c conda-forge jupyterlab


conda deactivate


