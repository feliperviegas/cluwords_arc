### EC2 Settings

- g4dn.xlarge
- **OS:** Deep Learning (Ubuntu 18.04)

### Download MiniCoda and install MiniConda

```
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
```

```
bash Miniconda3-latest-Linux-x86_64.sh
```

### Source the bashrc file to set up correct paths
```
source ~/.bashrc
```

### Create conda environment
```
conda create -n pytorch0.3 python=3.6
```

### Source activate environment
```
source activate pytorch0.3
```

### Install torch GPU

```
pip3 install torch==1.9.0+cu111 torchvision==0.10.0+cu111 torchaudio==0.9.0 -f https://download.pytorch.org/whl/torch_stable.html
```

### Install the requirements
```
pip3 install -r requirements.txt
```

### Install spacy english language

```
python3 -m spacy download en_core_web_sm
```
