# Samples of executions of CluSent

```
for dataset in  aisopos_ntua_2L; do \
       python3 main_random_search.py -d ${dataset} -t 2LabelValSample/${dataset}/texts_pre.txt \
               -l 2LabelValSample/${dataset}/score_pre.txt \
               -s 2LabelValSample/${dataset}/splits/split_5_with_val.pkl \
               -f 5 \
               -e bert_embeddings/word-embedding_${dataset} \
               -ed 3072\
               -le vader_sentiment_lexicon_v2.txt; \
done
```

**Script that performs feature compression:**
```
for dataset in  aisopos_ntua; do \
       python3 main_random_search_compression.py -d ${dataset} -t 2Label/${dataset}/texts_pre.txt \
               -l 2Label/${dataset}/score_pre.txt \
               -s 2Label/${dataset}/representations/split_5.csv \
               -f 5 \
               -e wiki-news-300d-1M.vec \
               -ed 300\
               -le vader_sentiment_lexicon_v2.txt; \
done
```



**Script to run a single fold id:**

```
for dataset in  aisopos_ntua; do \
       python3 main_single_fold.py -d ${dataset} -t 2Label/${dataset}/texts_pre.txt \
               -l 2Label/${dataset}/score_pre.txt \
               -s 2Label/${dataset}/representations/split_5.csv \
               -f 0 \
               -e wiki-news-300d-1M.vec \
               -ed 300\
               -le vader_sentiment_lexicon_v2.txt; \
done
```


### Script for preprocessing the dataset:

```
dataset="aisopos_ntua"; 
python3 src/main/python/preprocess/parse_splits.py -t 2Label/${dataset}/texts.txt \
-l 2Label/${dataset}/score_pre.txt \
-s 2Label/${dataset}/representations/split_5.csv \
-f 0 \
-p . \
-o 2Label/${dataset}/texts_pre.txt;
```

