import os
from os import path
from collections import Counter
import argparse
import numpy as np
import scipy.sparse as sp
import logging
import logging.config
import timeit
from src.main.python.embedding.create_fasttext_embedding import FastTextEmbedding
from src.main.python.ml_methods.classifiers import Classifier
from src.main.python.ml_methods.bayesiancv_svm import BayesSearchSvm
from src.main.python.ml_methods.bayesiancv_lightgbm import BayesSearchLightGbm
from src.main.python.preprocess.parse_splits import ParseRaw
from src.main.python.preprocess.document_vectorizer import DocumentVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from src.main.python.steps import clustering_step
from src.main.python.steps.clustering_step import ClusteringStep
from setting_env import EnvVariables
from src.main.python.steps.weighting_step import WeightingStep
from src.main.python.evaluation.metric import Evaluation
from src.main.python.weighting.grid_search import GridSearch
from src.main.python.weighting.bayesian_cv import BayesianCv
from src.main.python.utilities.utils import get_density

from src.main.python.embedding.bert_embedding import BertEmbedding


class InputParameters(object):
    def __init__(self, dataset_name, texts, labels, splits, fold, embedding, embedding_dimension, lexicons):
        self.dataset_name = dataset_name
        self.texts = texts
        self.labels = labels
        self.splits = splits
        self.fold = fold
        self.embedding = embedding
        self.embedding_dimension = embedding_dimension
        self.lexicons = lexicons

    def get_dataset_name(self):
        return self.dataset_name

    def get_texts(self):
        return self.texts

    def get_labels(self):
        return self.labels

    def get_splits(self):
        return self.splits

    def get_fold(self):
        return self.fold

    def get_embedding(self):
        return self.embedding

    def get_embedding_dimension(self):
        return self.embedding_dimension

    def get_lexicons(self):
        return self.lexicons


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset_name',
                        action='store',
                        type=str,
                        dest='dataset_name',
                        required=True,
                        help='-d [dataset folder name]')
    parser.add_argument('-t', '--texts',
                        action='store',
                        type=str,
                        dest='texts',
                        help='-t [texts folder name]')
    parser.add_argument('-l', '--labels',
                        action='store',
                        type=str,
                        dest='labels',
                        help='-l [labels folder name]')
    parser.add_argument('-e', '--embedding',
                        action='store',
                        type=str,
                        dest='embedding',
                        default=None,
                        help='-e [embedding file name]')
    parser.add_argument('-ed', '--embedding_dimension',
                        action='store',
                        type=int,
                        dest='embedding_dimension',
                        default=None,
                        help='-ed [embedding path]')
    parser.add_argument('-s', '--split',
                        action='store',
                        type=str,
                        default='',
                        dest='split',
                        help='-s [splits folder name]')
    parser.add_argument('-f', '--fold',
                        action='store',
                        type=int,
                        dest='fold',
                        required=True,
                        help='--fold [TRAIN/TEST FOLD]')
    parser.add_argument('-le', '--lexicons',
                        action='store',
                        type=str,
                        dest='lexicons',
                        required=False,
                        default=None,
                        help='--lexicons [lexicons]')
    args = parser.parse_args()

    env_variables = EnvVariables()
    input_params = InputParameters(dataset_name=args.dataset_name,
                                   texts=args.texts,
                                   labels=args.labels,
                                   splits=args.split,
                                   fold=args.fold,
                                   embedding=args.embedding,
                                   embedding_dimension=args.embedding_dimension,
                                   lexicons=args.lexicons)

    path_to_save_embeddings = f"{env_variables.get_path_to_save_embeddings()}/{input_params.get_dataset_name()}"
    path_to_save_representation = f"{env_variables.get_path_to_save_datasets()}/{input_params.get_dataset_name()}"
    # Create paths to save the experiments
    os.makedirs(f"{path_to_save_embeddings}", exist_ok=True)
    os.makedirs(f"{path_to_save_representation}", exist_ok=True)
    os.makedirs(f"/cluwords/tmp/", exist_ok=True)

    logging.config.fileConfig('logging.conf')

    # create logger
    logger = logging.getLogger(__name__)

    # #Create the word2vec models for each dataset
    logger.info("Filter embedding space to {} dataset.".format(input_params.get_dataset_name()))

    dataset_path = None
    logger.info(f"Vocab type {env_variables.get_vocab_type()}.")
    logger.info(f"Mode {env_variables.get_mode()}.")
    try:
        if env_variables.get_vocab_type():
            logger.info('Vocabulary considered in the Embedding Filter Collection.')
            dataset_path = input_params.get_texts()

        if env_variables.get_mode() == "sparse" or env_variables.get_mode() == "memmap":
            logger.info("Mode loaded.")
        else:
            raise NameError("Setting not found. Structure Mode not found, check the .env file for MODE variable.")
    except NameError:
        raise

    word_vectors = None
    vocabulary_embedding = None
    if dataset_path is input_params.get_texts():
        start = timeit.default_timer()
        fasttext_embedding = FastTextEmbedding(embedding_dimension=input_params.get_embedding_dimension(),
                                               embedding_file_path=input_params.get_embedding(),
                                               embedding_type=env_variables.get_embedding_type(),
                                               dataset_path=dataset_path,
                                               path_to_save_model=path_to_save_representation)

        n_words = fasttext_embedding.get_n_words()
        word_vectors, vocabulary_embedding = fasttext_embedding.get_word_vectors()
        end = timeit.default_timer()
        logger.info(f"Loaded Embedding considering the vocabulary of the collection {end - start}.")

    for fold in range(0, args.fold):
        start = timeit.default_timer()
        ParseRaw(split_file=args.split,
                 document_file=args.texts,
                 label_file=args.labels,
                 fold=fold,
                 save_path=path_to_save_representation).run(apply_preprocess=False)
        end = timeit.default_timer()
        logger.info(f"Loaded train/test sets fold ({fold}) {end - start}.")

        if env_variables.get_vocab_type() == 0:
            logger.info('Vocabulary considered in the Embedding Filter Training set.')
            dataset_path = '{path}/d_train_data_{fold}.txt'.format(path=path_to_save_representation,
                                                                   fold=fold)                                                     
        
        if dataset_path is not input_params.get_texts():
            start = timeit.default_timer()
            try:
                # Get the BERT tokenization
                bert = BertEmbedding()
                bert_vocabulary = bert.get_vocabulary(dataset_path=input_params.get_texts())
                
                fasttext_embedding = FastTextEmbedding(embedding_dimension=input_params.get_embedding_dimension(),
                                                    #    embedding_file_path=f"{input_params.get_embedding()}_{fold}_embeddings.csv",
                                                       embedding_file_path=f"{input_params.get_embedding()}-{fold}.txt",
                                                       embedding_type=env_variables.get_embedding_type(),
                                                       dataset_path=dataset_path,
                                                       path_to_save_model=path_to_save_representation,
                                                       vocabulary=bert_vocabulary)

                n_words = fasttext_embedding.get_n_words()
                word_vectors, vocabulary_embedding = fasttext_embedding.get_word_vectors()
                end = timeit.default_timer()
                logger.info(f"Loaded BERT Embedding considering the vocabulary of the training set {end - start}.")
                logger.info(f"Word Embeddings Shape {word_vectors.shape}.")

            except Exception as err:
                raise err

        try:
            if word_vectors is None:
                raise NameError("Not found word_vectors variable.")

            if vocabulary_embedding is None:
                raise NameError("Not found vocabulary_embedding variable.")

        except NameError:
            raise

        logger.info(f'Number of words (n_words) {n_words}.')
        nearest_neighbors = 500 if n_words > 500 else n_words

        logger = logging.getLogger("ClusteringStep")
        start = timeit.default_timer()
        try:
            cluster_step = ClusteringStep()
            cluster_step.run(path_to_save_results=path_to_save_embeddings,
                                                        n_threads=env_variables.get_n_jobs(),
                                                        k=nearest_neighbors,
                                                        threshold=0.4,
                                                        algorithm_type=env_variables.get_clustering_method(),
                                                        # fold=input_params.get_fold(),
                                                        fold=fold,
                                                        dataset=input_params.get_dataset_name(),
                                                        mode=env_variables.get_mode(),
                                                        word_vectors=word_vectors,
                                                        vocabulary_embedding=vocabulary_embedding)
        except Exception as err:
            raise err

        end = timeit.default_timer()

        logger.info(f"Evaluated threshold: {cluster_step.get_threshold()}.")

        logger.info(f"Clustering step ({env_variables.get_clustering_method()})  {end - start}.")

        start = timeit.default_timer()

        # Vectorizing Traning Set and Test Set
        logger = logging.getLogger("VectorizerStep")
        simple_vectorizer = DocumentVectorizer()

        vectorizer = CountVectorizer(analyzer='word',
                                     vocabulary=vocabulary_embedding,
                                     lowercase=False,
                                     token_pattern='[a-zA-Z0-9$&+,:;=?@#|<>.^*()%!-]+')

        training_docs = simple_vectorizer.read_raw_data(
            document_path="{path}/d_train_data_{fold}.txt".format(
                                                                  path=path_to_save_representation,
                                                                  fold=fold)
        )
        vectorizer.fit(training_docs)
        X_train = vectorizer.transform(training_docs)
        
        test_docs = simple_vectorizer.read_raw_data(
            document_path="{path}/d_test_data_{fold}.txt".format(
                                                                  path=path_to_save_representation,
                                                                  fold=fold)
        )
        X_test = vectorizer.transform(test_docs)

        val_docs = simple_vectorizer.read_raw_data(
            document_path="{path}/d_val_data_{fold}.txt".format(
                                                                  path=path_to_save_representation,
                                                                  fold=fold)
        )
        X_val = vectorizer.transform(val_docs)

        training_labels = simple_vectorizer.read_raw_data(
            document_path="{path}/c_train_data_{fold}.txt".format(path=path_to_save_representation,
                                                                  fold=fold),
        )
        y_train = simple_vectorizer.as_numpy_array(training_labels)
        test_labels = simple_vectorizer.read_raw_data(
            document_path="{path}/c_test_data_{fold}.txt".format(path=path_to_save_representation,
                                                                 fold=fold),
        )
        y_test = simple_vectorizer.as_numpy_array(test_labels)

        val_labels = simple_vectorizer.read_raw_data(
            document_path="{path}/c_val_data_{fold}.txt".format(path=path_to_save_representation,
                                                                fold=fold),
        )
        y_val = simple_vectorizer.as_numpy_array(val_labels)

        del training_docs
        del training_labels
        del test_docs
        del test_labels
        del vectorizer
        del simple_vectorizer

        logger.info(f"Train: {X_train.shape}")
        logger.info(f"Train: {Counter(y_train)}")
        logger.info(f"Val: {X_val.shape}")
        logger.info(f"Val: {Counter(y_val)}")
        logger.info(f"Test: {X_test.shape}")
        logger.info(f"Test: {Counter(y_test)}")

        end = timeit.default_timer()
        logger.info(f"Vectorizer train/test sets {end - start}.")

        logger = logging.getLogger("GridSearchStep")
        start = timeit.default_timer()
        try:
            grid_search = GridSearch(instantiations=["cw", "cw_pos", "cw-tf_al", "cw-pos-tf_al"], with_compression=False)

            grid_search.run_val(X_train=X_train,
                                y_train=y_train,
                                X_val=X_val,
                                y_val=y_val,
                                dataset_name=input_params.get_dataset_name(),
                                fold=fold,
                                path_to_save_representation=path_to_save_representation,
                                path_to_save_embeddings=path_to_save_embeddings,
                                input_lexicons=input_params.get_lexicons())

        except Exception as err:
            raise err

        end = timeit.default_timer()
        logger.info(f"Search for instantiations {end - start}.")

        try:
            best_instantiation = grid_search.get_best_instantiation()
            logger.info(grid_search.log_instantiations())

        except Exception as err:
            raise err

        logger.info(f"Best Instantiation {best_instantiation}")

        logger = logging.getLogger("WeightingStep")
        start = timeit.default_timer()
        try:
            weighting_step = WeightingStep()
            weighting_step.fit(training_docs=sp.vstack((X_train, X_val), format='csr'),
                               npz_path=path_to_save_embeddings,
                               dataset_name=input_params.get_dataset_name(),
                               input_lexicons=input_params.get_lexicons(),
                               fold=fold,
                               instantiation=best_instantiation)

            y_cw_train, x_cw_train = weighting_step.transform(X=sp.vstack((X_train, X_val), format='csr'),
                                                              y=np.concatenate((y_train, y_val), axis=0),
                                                              idf=False)

            logger.info(f"x_cw_train contains NaN: {np.isnan(x_cw_train).any()}")
            logger.info(f"x_cw_test contains Inf: {np.isinf(x_cw_train).any()}")

            y_cw_test, x_cw_test = weighting_step.transform(X=X_test,
                                                            y=y_test,
                                                            idf=False)

            logger.info(f"x_cw_test contains NaN: {np.isnan(x_cw_test).any()}")
            logger.info(f"x_cw_test contains Inf: {np.isinf(x_cw_test).any()}")

        except Exception as err:
            raise err

        ## To remove in the next merge
        # weighting_step.save_tf_idf_features_libsvm(cluwords_x=x_cw_train,
        #                                            cluwords_y=y_cw_train,
        #                                            path_to_save_cluwords=path_to_save_representation,
        #                                            file_name='train_{fold}'.format(fold=input_params.get_fold()),
        #                                            is_binary=False)
        #
        # weighting_step.save_tf_idf_features_libsvm(cluwords_x=x_cw_test,
        #                                            cluwords_y=y_cw_test,
        #                                            path_to_save_cluwords=path_to_save_representation,
        #                                            file_name='test_{fold}'.format(fold=input_params.get_fold()),
        #                                            is_binary=False)

        try:
            svm = Classifier(name='svm')
            svm_pred, svm_predict_proba = svm.run(x_train=x_cw_train,
                                                  y_train=y_cw_train,
                                                  x_test=x_cw_test,
                                                  y_test=y_cw_test,
                                                  fold=fold,
                                                  n_jobs=4,
                                                  path_to_save=path_to_save_representation)

            mean, min, max = get_density(x_cw_train)
            logger.info(f"Density of training set ({x_cw_train.shape[1]}): min: {min} max: {max} mean: {mean}")
            mean, min, max = get_density(x_cw_test)
            logger.info(f"Density of test set ({x_cw_test.shape[1]}): min: {min} max: {max} mean: {mean}")

            svm.write_output(filename_input='{}/res_svm_{}.txt'.format(path_to_save_representation,
                                                                       input_params.get_dataset_name()),
                             documents_label=y_cw_test,
                             y_proba=svm_predict_proba,
                             y_pred=svm_pred,
                             fold=fold)

        except Exception as err:
            raise err

        end = timeit.default_timer()
        logger.info(f"Weighting step for the chosen instantiation {end - start}.")

    macro_f1 = Evaluation().read_path(path='{}/res_svm_{}.txt'.format(path_to_save_representation,
                                                                      input_params.get_dataset_name()),
                                      type="macro")
    micro_f1 = Evaluation().read_path(path='{}/res_svm_{}.txt'.format(path_to_save_representation,
                                                                      input_params.get_dataset_name()),
                                      type="micro")


    logger.info(f"MacroF1 Results:")
    logger.info(f"Each fold:")
    for mac in macro_f1:
        logger.info(f"{mac}")
    logger.info(f"Average: {round(np.mean(macro_f1) * 100, 4)} {round(np.std(macro_f1, ddof=1) * 100, 4)}")
    logger.info(f"MicroF1 Results:")
    logger.info(f"Each fold:")
    for mic in micro_f1:
        logger.info(f"{mic}")
    logger.info(f"Average {round(np.mean(micro_f1) * 100, 4)} {round(np.std(micro_f1, ddof=1) * 100, 4)}")


if __name__ == '__main__':
    main()
